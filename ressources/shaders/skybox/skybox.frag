#version 130

in vec3 position;

out vec4 fragColor;

uniform samplerCube skybox;

void main(void)
{
	fragColor = texture(skybox, position);
}