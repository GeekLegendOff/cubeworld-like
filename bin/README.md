# CubeWorld-Like

Cubeworld Like is a third person multi-player RPG game set in a completely open voxel world. The map is procedurally generated 

The game is in actually development

![Preview](https://image.noelshack.com/fichiers/2019/43/4/1571872759-screenshot-8.png "Preview")

# Teams

- GeekLegend - Lead programmer
- Swiiz - Programmer
- Sanchobaya - Programmer
- Connaught - Programmer
- BelKa3 - Model maker
- FalconEy3 - Sound designer
