package engine.inputs;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class Input {
	
	private static List<Integer> keys = new ArrayList<Integer>();
	private static List<Integer> keysDown = new ArrayList<Integer>();
	
	private static List<Integer> buttons = new ArrayList<Integer>();
	private static List<Integer> buttonsDown = new ArrayList<Integer>();
	
	public static void update() {
		keysDown.clear();
		
		for (int i = 0; i < 256; i++) {
			if (getKey(i) && !keys.contains(i)) {
				keysDown.add(i);
			}
		}
		
		keys.clear();
		
		for (int i = 0; i < 256; i++) {
			if (getKey(i)) {
				keys.add(i);
			}
		}
		
		buttonsDown.clear();
		
		for (int i = 0; i < 5; i++) {
			if (getMouse(i) && !buttons.contains(i)) {
				buttonsDown.add(i);
			}
		}
		
		buttons.clear();
		
		for (int i = 0; i < 5; i++) {
			if (getMouse(i)) {
				buttons.add(i);
			}
		}
	}
	
	public static boolean getKeyDown(int key) {
		return keysDown.contains(key);
	}
	
	public static boolean getKey(int key) {
		return Keyboard.isKeyDown(key);
	}
	
	public static boolean getMouseDown(int m) {
		return buttonsDown.contains(m);
	}
	
	public static boolean getMouse(int m) {
		return Mouse.isButtonDown(m);
	}
}
