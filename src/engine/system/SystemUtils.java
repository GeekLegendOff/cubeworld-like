package engine.system;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SystemUtils
{
	
	public static String getDate()
	{
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
		return simpleDateFormat.format(date);
	}
	
	public static int getAvailableProcessors()
	{
		return Runtime.getRuntime().availableProcessors();
	}
	
	public static long getFreeMemory()
	{
		return Runtime.getRuntime().freeMemory();
	}
	
	public static long getMaxMemory()
	{
		return Runtime.getRuntime().maxMemory();
	}
	
	public static long getTotalMemory()
	{
		return Runtime.getRuntime().totalMemory();
	}

	public static OS getOS()
	{
		if (isWindows())
		{
			return OS.WINDOWS;
		} else if (isMac())
		{
			return OS.MACOSX;
		} else if (isUnix())
		{
			return OS.LINUX;
		} else
		{
			return OS.UNKNOWN;
		}
	}

	public static boolean isWindows()
	{
		return (getOSName().indexOf("win") >= 0 || getOSName().indexOf("Win") >= 0);
	}

	public static boolean isMac()
	{
		return (getOSName().indexOf("mac") >= 0 || getOSName().indexOf("Mac") >= 0);
	}

	public static boolean isUnix()
	{
		return (getOSName().indexOf("nix") >= 0 || getOSName().indexOf("nux") >= 0 || getOSName().indexOf("aix") > 0);
	}

	public static boolean isSolaris()
	{
		return (getOSName().indexOf("sunos") >= 0 || getOSName().indexOf("Sunos") >= 0);
	}

	public static String getOSName()
	{
		return System.getProperty("os.name");
	}

	public static String getOSVersion()
	{
		return System.getProperty("os.version");
	}

	public static String getOSArchitecture()
	{
		return System.getProperty("os.arch");
	}

	public static String getJavaVersion()
	{
		return System.getProperty("java.version");
	}

}
