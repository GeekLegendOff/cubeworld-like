package engine.system;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Log
{

	private static String path = "log.txt";

	public static void resetLog()
	{
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;
		try
		{
			fileWriter = new FileWriter(path, false);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write("New log file created at: " + SystemUtils.getDate());
			bufferedWriter.newLine();
			bufferedWriter.close();
		} catch (IOException exception)
		{
			System.err.println("Error: " + exception.getLocalizedMessage());
		} finally
		{
			try
			{
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException exception)
			{
				System.err.println(exception.getLocalizedMessage());
			}
		}
	}

	public static void addLine(String fileName, String text)
	{
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;
		try
		{
			fileWriter = new FileWriter(fileName, true);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(text);
			bufferedWriter.newLine();
			bufferedWriter.close();
		} catch (IOException exception)
		{
			System.err.println(exception.getLocalizedMessage());
		} finally
		{
			try
			{
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException exception)
			{
				System.err.println(exception.getLocalizedMessage());
			}
		}
	}

	public static void print(String message)
	{
		System.out.print(message);
		addLine(path, message);
	}

	public static void println(String message)
	{
		System.out.println(message);
		addLine(path, message);
	}

	public static void print(String prefix, String message)
	{
		System.err.print("[" + prefix + "]" + message);
		addLine(path, "[" + prefix + "]" + SystemUtils.getDate() + " > " + message);
	}

	public static void println(String prefix, String message)
	{
		System.err.println("[" + prefix + "]" + message);
		addLine(path, "[" + prefix + "]" + SystemUtils.getDate() + " > " + message);
	}

	public static void info(String info)
	{
		System.out.println("[INFO] " + info);
		addLine(path, "[INFO] " + SystemUtils.getDate() + " > " + info);
	}

	public static void error(String error)
	{
		System.out.println("[ERROR] " + error);
		addLine(path, "[ERROR] " + SystemUtils.getDate() + " > " + error);
	}

	public static void severe(String severe)
	{
		System.out.println("[SEVERE] " + severe);
		System.out.println("[CRASH] Application ended");
		addLine(path, "[SEVERE] " + SystemUtils.getDate() + " > " + severe);
		addLine(path, "[CRASH] " + SystemUtils.getDate() + " > Application ended");
		System.exit(1);
	}

	public static void crash(String error)
	{
		throw new RuntimeException(error);
	}

	public static void exception(Exception exception)
	{
		String crash = "======== Exception: " + exception.toString() + " =========";
		Log.addLine(path, "");
		Log.addLine(path, crash);
		for (int i = 0; i < exception.getStackTrace().length; i++)
			Log.addLine(path, "	at " + exception.getStackTrace()[i].getClassName() + " ("
					+ exception.getStackTrace()[i].getMethodName() + ":" + exception.getStackTrace()[i].getLineNumber() + ")");
		String eq = "";
		for (int i = 0; i < crash.length(); i++)
			eq += "=";
		Log.addLine(path, eq);
		Log.addLine(path, "");
		exception.printStackTrace();
	}

}
