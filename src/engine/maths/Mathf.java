package engine.maths;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class Mathf
{

	public static final float PI = 3.1415927f;

	public static float min(float a, float b)
	{
		if (a < b)
			return a;
		return b;
	}

	public static float max(float a, float b)
	{
		if (a > b)
			return a;
		return b;
	}

	public static float clamp(float min, float max, float value)
	{
		if (value < min)
			value = min;
		if (value > max)
		{
			value = max;
		}
		return value;
	}

	public static float cos(float angle)
	{
		return (float) Math.cos(angle);
	}

	public static float sin(float angle)
	{
		return (float) Math.sin(angle);
	}

	public static float tan(float angle)
	{
		return (float) Math.tan(angle);
	}

	public static float acos(float angle)
	{
		return (float) Math.acos(angle);
	}

	public static float asin(float angle)
	{
		return (float) Math.asin(angle);
	}

	public static float atan(float angle)
	{
		return (float) Math.atan(angle);
	}

	public static float atan2(float x, float y)
	{
		return (float) Math.atan2(x, y);
	}

	public static float toRadians(float angle)
	{
		return (float) Math.toRadians(angle);
	}
	
	public static Vector3f toRadians(Vector3f v)
	{
		Vector3f r = new Vector3f();
		r.x = (float) Math.toRadians(v.x);
		r.y = (float) Math.toRadians(v.y);
		r.z = (float) Math.toRadians(v.z);
		return r;
	}
	
	public static Vector2f toRadians(Vector2f v)
	{
		Vector2f r = new Vector2f();
		r.x = (float) Math.toRadians(v.x);
		r.y = (float) Math.toRadians(v.y);
		return r;
	}

	public static float toDegrees(float angle)
	{
		return (float) Math.toDegrees(angle);
	}

	public static float sqrt(float a)
	{
		return (float) Math.sqrt(a);
	}

	public static float floor(float a)
	{
		return (float) Math.floor(a);
	}

	public static float ceil(float a)
	{
		return (float) Math.ceil(a);
	}

	public static float round(float a)
	{
		return Math.round(a);
	}

	public static float random()
	{
		return (float) Math.random();
	}

	public static float random(float a, float b)
	{
		return a + random() * (b - a);
	}

	public static float abs(float a)
	{
		return Math.abs(a);
	}

	public static float pow(float f, float p)
	{
		return (float) Math.pow(f, p);
	}

	public static float lerp(float s, float e, float t)
	{
		return s + (e - s) * t;
	}

	public static float cLerp(float s, float e, float t)
	{
		float result = (1.0F - (float) Math.cos(t * Math.PI)) / 2.0F;
		return s * (1.0F - result) + e * result;
	}

	public static float sLerp(float s, float e, float t)
	{
		float result = (1.0F - (float) Math.sin(t * Math.PI)) / 2.0F;
		return s * (1.0F - result) + e * result;
	}

	public static float bLerp(float c00, float c10, float c01, float c11, float tx, float ty)
	{
		return cLerp(cLerp(c00, c10, tx), cLerp(c01, c11, tx), ty);
	}

}
