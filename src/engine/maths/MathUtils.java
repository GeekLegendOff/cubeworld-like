package engine.maths;

public class MathUtils
{
	
	public float x, y, z;
	
	public MathUtils(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public MathUtils check()
	{
		float max = Math.max(Math.max(this.x, this.y), this.z);
		float min = Math.min(Math.min(this.x, this.y), this.z);

		float absMax = Math.abs(max - 1.0F);
		float absMin = Math.abs(min);

		float v = 0.0F;
		if (absMax > absMin)
		{
			v = min;
		} else
		{
			v = max;
		}
		int rv = 1;

		if (v < 0.5F)
			rv = -1;

		return new MathUtils((v == this.x) ? rv : 0.0F, (v == this.y) ? rv : 0.0F, (v == this.z) ? rv : 0.0F);
	}

}
