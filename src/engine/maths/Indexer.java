package engine.maths;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class Indexer
{
	
	public static int index3i(int x, int y, int z)
	{
		return x << 16 | y << 8 | z;
	}
	
	public static Vector3f solve3i(int index)
	{
		int x = (index & 0xFF0000) >> 16;
		int y = (index & 0xFF00) >> 8;
		int z = index & 0xFF;
		
		return new Vector3f(x, y, z);
	}
	
	public static long pack(int x, int y)
	{
		long xPacked = x << 32;
		long yPacked = y & 0xFFFFFFFFL;
		
		return xPacked | yPacked;
	}
	
	public static Vector2f unpack(long packed)
	{
		return new Vector2f((float) (packed >> 32), (float) (packed & 0xFFFFFFFFL));
	}

}
