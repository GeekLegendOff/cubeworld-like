package engine.rendering.guis.manager;

import java.util.ArrayList;
import java.util.List;

import engine.rendering.guis.GuiButton;
import engine.rendering.guis.GuiLabel;

public class GuiManager
{
	
	private static List<GuiLabel> labels = new ArrayList<GuiLabel>();
	private static List<GuiButton> buttons = new ArrayList<GuiButton>();
	
	public void render()
	{
		for (GuiLabel label : labels)
		{
			label.render();
		}
		for (GuiButton button : buttons)
		{
			button.render();
		}
	}
	
	public void update()
	{
		for (GuiLabel label : labels)
		{
			label.update();
		}
		for (GuiButton button : buttons)
		{
			button.update();
		}
	}
	
	public static void addLabel(GuiLabel label)
	{
		if (!labelsContains(label))
		{
			labels.add(label);
		}
	}
	
	public static void removeLabel(GuiLabel button)
	{
		if (labelsContains(button))
		{
			labels.remove(button);
		}
	}
	
	public static boolean labelsContains(GuiLabel label)
	{
		return labels.contains(label);
	}
	
	public static List<GuiLabel> getLabels()
	{
		return labels;
	}
	
	public static void addButton(GuiButton button)
	{
		if (!buttonsContains(button))
		{
			buttons.add(button);
		}
	}
	
	public static void removeButton(GuiButton button)
	{
		if (buttonsContains(button))
		{
			buttons.remove(button);
		}
	}
	
	public static boolean buttonsContains(GuiButton button)
	{
		return buttons.contains(button);
	}
	
	public static List<GuiButton> getButtons()
	{
		return buttons;
	}

}
