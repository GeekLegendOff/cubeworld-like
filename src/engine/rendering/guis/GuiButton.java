package engine.rendering.guis;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import engine.rendering.Renderer;
import engine.utils.Color4f;

public class GuiButton extends Gui
{

	private String name;
	private int size;
	private TrueTypeFont trueTypeFont;
	private Font font;

	public GuiButton(float x, float y, float width, float height, String name, int size)
	{
		super(x, y, width, height);
		this.name = name;
		this.size = size;
		this.font = new Font(name, Font.BOLD, size);
		this.trueTypeFont = new TrueTypeFont(font, false);
	}

	@Override
	public void render()
	{
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
		
		Renderer.quadGui(x, y, width, height, Color4f.DARKGRAY_ALPHA);

		glDisable(GL_BLEND);
		
		trueTypeFont.drawString(x, y, name, Color.white);
	}

	@Override
	public void update()
	{

	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getSize()
	{
		return size;
	}

	public void setSize(int size)
	{
		this.size = size;
	}

	public TrueTypeFont getTrueTypeFont()
	{
		return trueTypeFont;
	}

	public Font getFont()
	{
		return font;
	}

}
