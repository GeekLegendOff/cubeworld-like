package engine.rendering.guis;

import static org.lwjgl.opengl.GL11.*;

import engine.rendering.Renderer;
import engine.rendering.font.Font;
import engine.utils.Color4f;

public class GuiLabel extends Gui
{

	private String name;
	private int size;

	public GuiLabel(float x, float y, float width, float height, String name, int size)
	{
		super(x, y, width, height);
		this.name = name;
		this.size = size;
	}

	@Override
	public void render()
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		Renderer.renderGui(x, y, width, height, Color4f.DARKGRAY_ALPHA);

		glEnable(GL_TEXTURE_2D);
		
		Font.drawString(name, x, y, size);
		
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
	}

	@Override
	public void update()
	{
		
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getSize()
	{
		return size;
	}

	public void setSize(int size)
	{
		this.size = size;
	}

}
