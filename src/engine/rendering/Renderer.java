package engine.rendering;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.Display;

import engine.utils.Color4f;

public class Renderer
{

	public static void quadGui(float x, float y, float width, float height, Color4f color)
	{
		glColor4f(color.r, color.g, color.b, color.a);
		
		glVertex2f(x, y);
		glVertex2f(x + width, y);
		glVertex2f(x + width, y + height);
		glVertex2f(x, y + height);
	}
	
	public static void renderGui(float x, float y, float width, float height, Color4f color)
	{
		glBegin(GL_QUADS);
		quadGui(x, y, width, height, color);
		glEnd();
	}
	
	public static void ortho()
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(70.0f, Display.getWidth(), Display.getHeight(), 0, 0, 1);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}

}
