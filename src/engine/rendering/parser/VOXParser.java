package engine.rendering.parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class VOXParser
{

	public static int[][][] parse(String path)
	{
		return parse(new String[]
		{ path });
	}

	public static int[][][] parse(String[] path)
	{
		int targetXSize = 0;
		int targetYSize = 0;
		int targetZSize = 0;
		int[][][] modelData = new int[0][][];

		for (int p = 0; p < path.length; p++)
		{
			Path pathToFile = Paths.get(path[p]);
			byte[] data = new byte[0];
			try
			{
				data = Files.readAllBytes(pathToFile);
			} catch (IOException e)
			{
				e.printStackTrace();
			}

			int zSize = 0;
			int xSize = 0;
			int ySize = 0;

			boolean readStart = true;
			boolean readSize = false;
			boolean readCount = false;
			boolean readData = false;
			int counter = 0;
			for (int b = 0; b < data.length; b += 4)
			{
				int b1 = data[b] & 0xFF;
				int b2 = data[b + 1] & 0xFF;
				int b3 = data[b + 2] & 0xFF;
				int b4 = data[b + 3] & 0xFF;

				if (readStart)
				{
					if (b1 == 83 && b2 == 73 && b3 == 90 && b4 == 69)
					{
						b += 8;
						counter = 0;
						readStart = false;
						readSize = true;
					}
				} else if (readSize)
				{
					int size = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
					if (counter == 0)
						zSize = size;
					else if (counter == 1)
						xSize = size;
					else
						ySize = size;
					counter++;
					if (counter >= 3)
					{
						if (p == 0)
						{
							targetXSize = xSize;
							targetYSize = ySize;
							targetZSize = zSize;
							modelData = new int[targetXSize][targetYSize][targetZSize];
						} else if (targetXSize != xSize || targetYSize != ySize || targetZSize != zSize)
						{
							System.err.println("VOX data combination size mismatch for file " + path[p]);
						}
						b += 12;
						counter = 0;
						readSize = false;
						readCount = true;
					}
				} else if (readCount)
				{
					counter = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
					readCount = false;
					readData = true;
				} else if (readData)
				{
					modelData[b2][b3][b1] = b4;
					counter--;
					if (counter <= 0)
						break;
				}
			}
		}
		return modelData;
	}

}