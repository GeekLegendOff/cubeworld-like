package engine.utils;

import java.util.Random;

public class Color4f
{

	public static final Color4f WHITE = new Color4f(1.0f, 1.0f, 1.0f, 1.0f);
	public static final Color4f BLACK = new Color4f(0.0f, 0.0f, 0.0f, 1.0f);
	public static final Color4f DARKGRAY_ALPHA = new Color4f(4.0f, 4.0f, 4.0f, 0.5f);

	public float r, g, b, a;

	private static Random random = new Random();

	public Color4f(float r, float g, float b, float a)
	{
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public Color4f(float r, float g, float b)
	{
		this(r, g, b, 1);
	}

	public Color4f setColor(float r, float g, float b, float a)
	{
		return new Color4f(r, g, b, a);
	}

	public Color4f setColor(float r, float g, float b)
	{
		return new Color4f(r, g, b);
	}

	public static Color4f getRandomColor()
	{
		return new Color4f(random.nextFloat(), random.nextFloat(), random.nextFloat());
	}

	public Color4f(int rgb)
	{
		int r = (rgb & 0xFF0000) >> 16;
		int g = (rgb & 0xFF00) >> 8;
		int b = rgb & 0xFF;

		this.r = r / 255.0f;
		this.g = g / 255.0f;
		this.b = b / 255.0f;
		this.a = 1.0f;
	}

	public static Color4f mix(Color4f ca, Color4f cb, float t)
	{
		float r = Math.abs(ca.r + (cb.r - ca.r) * t);
		float g = Math.abs(ca.g + (cb.g - ca.g) * t);
		float b = Math.abs(ca.b + (cb.b - ca.b) * t);

		return new Color4f(r, g, b, ca.getA());
	}

	public Color4f add(float v)
	{
		this.r += v;
		this.g += v;
		this.b += v;
		return this;
	}

	public Color4f add(Color4f v)
	{
		this.r += v.r;
		this.g += v.g;
		this.b += v.b;
		return this;
	}

	public int getARGB()
	{
		int a = (int) (this.a * 127.0f);
		int r = (int) (this.r * 255.0f);
		int g = (int) (this.g * 255.0f);
		int b = (int) (this.b * 255.0f);

		return a << 24 | r << 16 | g << 8 | b;
	}

	public static Color4f getColorFromARGB(int argb)
	{
		Color4f color = new Color4f(0.0f, 0.0f, 0.0f, 0.0f);
		
		int a = (argb & 0xFF000000) >> 24;
		int r = (argb & 0xFF0000) >> 16;
		int g = (argb & 0xFF00) >> 8;
		int b = argb & 0xFF;

		color.r = r / 255.0f;
		color.g = g / 255.0f;
		color.b = b / 255.0f;
		color.a = a / 127.0f;

		return color;
	}

	public float getR()
	{
		return r;
	}

	public void setR(float r)
	{
		this.r = r;
	}

	public float getG()
	{
		return g;
	}

	public void setG(float g)
	{
		this.g = g;
	}

	public float getB()
	{
		return b;
	}

	public void setB(float b)
	{
		this.b = b;
	}

	public float getA()
	{
		return a;
	}

	public void setA(float a)
	{
		this.a = a;
	}

}
