package engine.game;

public interface IGameLogic
{
	
	void init();
	
	void render();
	
	void update();

	void cleanUp();
	
}
