package game.entities;

import org.joml.Vector2f;
import org.joml.Vector3f;

import engine.game.IGameLogic;
import game.utils.Pair;
import game.world.WorldManager;

public abstract class Entity implements IGameLogic
{

	public Vector3f position;
	public Vector2f rotation;
	public String name;
	public float height;
	public int id;
	public int health;
	public float gravityFactor;
	public boolean grounded, gravity, flyMode;
	public Pair<Boolean, Integer> jump;

	public Entity(Vector3f position, String name, float height, int id)
	{
		this.position = position;
		this.rotation = new Vector2f();
		this.name = name;
		this.height = height;
		this.id = id;
		this.gravityFactor = 0.0f;
		this.gravity = true;
		this.flyMode = false;
		this.jump = new Pair<>(false, 0);
	}
	
	public void move(float x, float y, float z, WorldManager worldManager)
	{
		if (gravity && !flyMode)
		{
			if(jump.key && jump.value < 0) {
				gravityFactor = 0.15f + 0.35f * -jump.value/100;
			}
			else {
				gravityFactor = 0.15f;
			}
			
			if (grounded)
			{
				gravityFactor = 0.1f;
			}
			y -= gravityFactor;
		}
		
		if (!isColliding(x, 0.0f, 0.0f, worldManager)) position.x += x;
		if (!isColliding(0.0f, y, 0.0f, worldManager)) position.y += y;
		if (!isColliding(0.0f, 0.0f, z, worldManager)) position.z += z;
		
		if (!isGrounded(0.0f, y, 0.0f, worldManager))
		{
			grounded = false;
		} else
		{
			grounded = true;
		}
	}
	
	public boolean isColliding(float x, float y, float z, WorldManager worldManager)
	{
		if(flyMode)
			return false;
		
		float radius = 0.17f;
		
		int x0 = (int) (position.x + x - radius);
		int x1 = (int) (position.x + x + radius);
		
		int y0 = (int) (position.y + y - radius);
		int y1 = (int) (position.y + y + radius);

		int z0 = (int) (position.z + z - radius);
		int z1 = (int) (position.z + z + radius);
		
		if (worldManager.getBlock(x0, y0, z0) != null) return true;
		if (worldManager.getBlock(x1, y0, z0) != null) return true;
		if (worldManager.getBlock(x1, y1+1, z0) != null) return true;
		if (worldManager.getBlock(x0, y1+1, z0) != null) return true;
		if (worldManager.getBlock(x0, y0, z1) != null) return true;
		if (worldManager.getBlock(x1, y0, z1) != null) return true;
		if (worldManager.getBlock(x1, y1+1, z1) != null) return true;
		if (worldManager.getBlock(x0, y1+1, z1) != null) return true;
		
		return false;
	}
	
	public boolean isGrounded(float x, float y, float z, WorldManager worldManager)
	{
		float radius = 0.17f;
		
		int y0 = (int) (position.y + y - radius);
		
		int xmid = (int) (position.x + radius);
		int zmid = (int) (position.z + radius);
		
		if (worldManager.getBlock(xmid, y0, zmid) != null)
		{
			return true;
		}
		return false;
	}

	public Vector3f getPosition()
	{
		return position;
	}
	
	public void setPosition(Vector3f position)
	{
		this.position = position;
	}
	
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public float getHeight()
	{
		return height;
	}
	
	public void setHeight(float height)
	{
		this.height = height;
	}
	
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

}
