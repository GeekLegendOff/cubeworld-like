package game.entities.manager;

import java.util.ArrayList;
import java.util.List;

import engine.game.IGameLogic;
import game.entities.Entity;

public class EntityManager implements IGameLogic
{

	private List<Entity> entities;

	public EntityManager()
	{
		this.entities = new ArrayList<Entity>();
	}

	@Override
	public void init()
	{
	}

	public void add(Entity entity)
	{
		if (!contains(entity))
		{
			entities.add(entity);
		}
	}

	public void remove(Entity entity)
	{
		if (contains(entity))
		{
			entities.remove(entity);
		}
	}

	@Override
	public void render()
	{
		for (Entity entity : entities)
		{
			if (contains(entity))
			{
				entity.render();
			}
		}
	}

	@Override
	public void update()
	{
		for (Entity entity : entities)
		{
			if (contains(entity))
			{
				entity.update();
			}
		}
	}

	@Override
	public void cleanUp()
	{
		for (Entity entity : entities)
		{
			if (contains(entity))
			{
				entity.cleanUp();
			}
		}
	}

	public boolean contains(Entity entity)
	{
		return entities.contains(entity);
	}

	public List<Entity> getEntities()
	{
		return entities;
	}

}
