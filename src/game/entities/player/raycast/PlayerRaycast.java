package game.entities.player.raycast;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3f;

import game.entities.player.Player;
import game.world.WorldManager;

public class PlayerRaycast
{

	private Player player;
	private List<Vector3f> points;

	public PlayerRaycast(Player player)
	{
		this.player = player;
		this.points = new ArrayList<Vector3f>();
		
		for (int i = 0; i < 5 * 16; i++)
		{
			points.add(new Vector3f());
		}
	}
	
	public void update()
	{
		int i = 0;
		for (Vector3f v : points)
		{
			Vector3f position = player.getPosition().add(player.getForward().mul(i / 16.0f));
			v.set(position);
			i++;
		}
	}
	
	public Vector3f getBlock(WorldManager worldManager)
	{
		for (Vector3f v : points)
		{
			float x = v.x;
			float y = v.y;
			float z = v.z;
			
			boolean block = worldManager.getBlock((int) x, (int) y, (int) z) != null;
			
			if (block || y <= 0)
			{
				return new Vector3f(x, y, z);
			}
		}
		return null;
	}
	
}
