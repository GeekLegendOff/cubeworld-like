package game.entities.player;

import static org.lwjgl.opengl.GL11.*;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.glu.GLU;

import engine.inputs.Input;
import game.Game;
import game.entities.Entity;
import game.entities.player.raycast.PlayerRaycast;
import game.particules.ParticuleManager;
import game.rendering.cullings.DistanceCulling;
import game.rendering.debugscreen.DebugScreen;
import game.utils.Pair;

public class Player extends Entity
{

	private float movementSpeed, mouseSpeed;

	private float fov, zNear, zFar;
	
	private int selectedBlock;
	private Vector3f selectedPosition;
	
	private PlayerRaycast playerRaycast;
	
	private DistanceCulling distanceCulling;

	private DebugScreen debugScreen;
	
	private ParticuleManager particuleManager;
	
	public Player(Vector3f position, int id, String name, float height)
	{
		super(position, name, height, id);
		this.movementSpeed = 0.2f;
		this.mouseSpeed = 0.3f;
		this.selectedBlock = 0;
		this.selectedPosition = new Vector3f();
		this.playerRaycast = new PlayerRaycast(this);
		this.distanceCulling = new DistanceCulling();
		this.debugScreen = new DebugScreen(Display.getWidth(), Display.getHeight());
		this.particuleManager = new ParticuleManager(/* Multiplier */ 1);
	}

	public Player setPerspectiveProjection(float fov, float zNear, float zFar)
	{
		this.fov = fov;
		this.zNear = zNear;
		this.zFar = zFar;

		return this;
	}

	@Override
	public void init()
	{
		debugScreen.init();
	}

	@Override
	public void render()
	{
		debugScreen.render();

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		GLU.gluPerspective(fov, (float) Display.getWidth() / (float) Display.getHeight(), zNear, zFar);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glPushAttrib(GL_TRANSFORM_BIT);
		glRotatef(rotation.x, 1, 0, 0);
		glRotatef(rotation.y, 0, 1, 0);
		glTranslatef(-position.x, -position.y - height, -position.z);
		glPopMatrix();
	}

	@Override
	public void update()
	{
		rotation.x -= Mouse.getDY() * mouseSpeed;
		rotation.y += Mouse.getDX() * mouseSpeed;

		Vector3f newV = new Vector3f(0, 0, 0);
		
		if (rotation.x >= 90)
		{
			rotation.x = 90;
		} else if (rotation.x <= -90)
		{
			rotation.x = -90;
		}
		
		if (Input.getKeyDown(Keyboard.KEY_F))
		{
			flyMode = !flyMode;
		}

		if (Input.getKey(Keyboard.KEY_Z) || Input.getKey(Keyboard.KEY_W))
		{
			newV.add(getForward().mul(movementSpeed));
		}
		if (Input.getKey(Keyboard.KEY_S))
		{
			newV.add(getBack().mul(movementSpeed));
		}
		if (Input.getKey(Keyboard.KEY_Q) || Input.getKey(Keyboard.KEY_A))
		{
			newV.add(getLeft().mul(movementSpeed));
		}
		if (Input.getKey(Keyboard.KEY_D))
		{
			newV.add(getRight().mul(movementSpeed));
		}
		if (Input.getKey(Keyboard.KEY_SPACE))
		{
			if (grounded && !flyMode)
			{
				this.jump = new Pair<>(true, 60);		
			} else if(flyMode)
			{
				newV.add(getUp().mul(movementSpeed));
			}
		}
		if (Input.getKey(Keyboard.KEY_LSHIFT))
		{
			if (flyMode)
			{
				newV.add(getDown().mul(movementSpeed));				
			}
		}
		
		if(jump.key) {
			if(jump.value >= 0)
				newV.add(getUp().mul(jump.value * 0.4f/100));
			jump.value--;

			if(grounded && jump.value < 0) {
				jump.key = false;
			}
		}
		

		move(newV.x, newV.y, newV.z, Game.getInstance().getGameRenderer().getWorldManager());
		
//		playerRaycast.update();
		
//		Vector3f raycastPosition = playerRaycast.getBlock(Game.getInstance().getGameRenderer().getWorldManager());
//		
//		if (raycastPosition != null)
//		{
//			selectedBlock = Game.getInstance().getGameRenderer().getWorldManager().getBlock((int) raycastPosition.x, (int) raycastPosition.y, (int) raycastPosition.z);
//			selectedPosition = new Vector3f(raycastPosition.x, raycastPosition.y, raycastPosition.z);
//		} else 
//		{
//			selectedPosition = new Vector3f();
//		}
//		
//		if (Input.getMouse(0))
//		{
//			Game.getInstance().getGameRenderer().getWorldManager().setBlock((int) selectedPosition.x, (int) selectedPosition.y, (int) selectedPosition.z, 0);
//			
//			if (Game.getInstance().getGameRenderer().getWorldManager().getChunk((int) selectedPosition.x, (int) selectedPosition.z) != null)
//			{
//				Game.getInstance().getGameRenderer().getWorldManager().updateChunk((int) selectedPosition.x, (int) selectedPosition.z);
//			}
//		} else if (Input.getMouse(1))
//		{
//			int x = (int) selectedPosition.x;
//			int y = (int) selectedPosition.y;
//			int z = (int) selectedPosition.z;
//			
//			float xx = selectedPosition.x;
//			float yy = selectedPosition.y;
//			float zz = selectedPosition.z;
//			
//			float vx = xx - x;
//			float vy = yy - y;
//			float vz = zz - z;
//
//			MathUtils check = new MathUtils(vx, vy, vz).check();
//			
//			int xp = (int) check.x;
//			int yp = (int) check.y;
//			int zp = (int) check.z;
//			
//			int rx = x + xp;
//			int ry = y + yp;
//			int rz = z + zp;
//			
//			Game.getInstance().getGameRenderer().getWorldManager().setBlock(rx, ry, rz, Block.WOOD.getARGB());
//			
//			if (Game.getInstance().getGameRenderer().getWorldManager().getChunk(rx, rz) != null)
//			{
//				Game.getInstance().getGameRenderer().getWorldManager().updateChunk(rx, rz);
//			}
//		}
		
		distanceCulling.update(position, 150.0f);

		debugScreen.update();
	}

	@Override
	public void cleanUp()
	{
		debugScreen.cleanUp();
	}

	public Vector3f getForward()
	{
		Vector3f r = new Vector3f();
		Vector2f rot = new Vector2f(rotation);

		float cosY = (float) Math.cos(Math.toRadians(rot.y - 90));
		float sinY = (float) Math.sin(Math.toRadians(rot.y - 90));
		float cosP = (float) Math.cos(Math.toRadians(-rot.x));

		r.x = cosY * cosP;
		r.y = 0;
		r.z = sinY * cosP;
		r.normalize();

		return new Vector3f(r);
	}

	public Vector3f getBack()
	{
		return new Vector3f(getForward().mul(-1));
	}

	public Vector3f getRight()
	{
		Vector3f r = new Vector3f();
		Vector2f rot = new Vector2f(rotation);

		r.x = (float) Math.cos(Math.toRadians(rot.y));
		r.z = (float) Math.sin(Math.toRadians(rot.y));
		r.normalize();

		return new Vector3f(r);
	}

	public Vector3f getLeft()
	{
		return new Vector3f(getRight().mul(-1));
	}

	public Vector3f getUp()
	{
		return new Vector3f(0, 1, 0);
	}

	public Vector3f getDown()
	{
		return new Vector3f(getUp().mul(-1));
	}

	public float getFov()
	{
		return fov;
	}

	public float getzNear()
	{
		return zNear;
	}

	public float getzFar()
	{
		return zFar;
	}

	public DistanceCulling getDistanceCulling()
	{
		return distanceCulling;
	}

}
