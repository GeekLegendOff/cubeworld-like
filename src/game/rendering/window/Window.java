package game.rendering.window;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class Window
{

	private int width, height;
	private String title;
	private boolean fullscreen, resizable, vsync;

	public Window(int width, int height, String title, boolean fullscreen, boolean resizable, boolean vsync)
	{
		this.width = width;
		this.height = height;
		this.title = title;
		this.fullscreen = fullscreen;
		this.resizable = resizable;
		this.vsync = vsync;
	}

	public void create()
	{
		try
		{
			Display.setDisplayMode(new DisplayMode(width, height));
			Display.setTitle(title);
			Display.setFullscreen(fullscreen);
			Display.setResizable(true);
			Display.setVSyncEnabled(vsync);
			Display.create();

			glEnable(GL_DEPTH_TEST);
		} catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}

	public void clearBuffers()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	public boolean isCloseRequested()
	{
		return Display.isCloseRequested();
	}

	public void update()
	{
		if (Display.wasResized())
		{
			glViewport(0, 0, Display.getWidth(), Display.getHeight());
		}

		Display.update();
	}

	public void destroy()
	{
		Display.destroy();
	}

	public int getWidth()
	{
		return width;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public int getHeight()
	{
		return height;
	}

	public void setHeight(int height)
	{
		this.height = height;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public boolean isFullscreen()
	{
		return fullscreen;
	}

	public void setFullscreen()
	{
		if (!fullscreen)
		{
			fullscreen = true;
			
			try
			{
				DisplayMode displayMode = Display.getDesktopDisplayMode();
				Display.setDisplayModeAndFullscreen(displayMode);

				glViewport(0, 0, displayMode.getWidth(), displayMode.getHeight());
			} catch (LWJGLException e)
			{
				e.printStackTrace();
			}
		} else
		{
			fullscreen = false;
			
			try
			{
				Display.setDisplayMode(new DisplayMode(width, height));
			} catch (LWJGLException e)
			{
				e.printStackTrace();
			}
		}
	}

	public boolean isResizable()
	{
		return resizable;
	}

	public void setResizable(boolean resizable)
	{
		this.resizable = resizable;
	}

	public boolean isVsync()
	{
		return vsync;
	}

	public void setVsync(boolean vsync)
	{
		this.vsync = vsync;
	}

}
