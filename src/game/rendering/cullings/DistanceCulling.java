package game.rendering.cullings;

import org.joml.Vector3f;

public class DistanceCulling
{
	
	private Vector3f cameraPosition;
	private float radius;
	
	public void update(Vector3f cameraPosition, float radius)
	{
		this.cameraPosition = cameraPosition;
		this.radius = radius * radius;
	}
	
	public boolean isInViewDistance(Vector3f position, float radius)
	{
		float xx = this.cameraPosition.x - position.x;
		float yy = this.cameraPosition.y - position.y;
		float zz = this.cameraPosition.z - position.z;
		
		return (xx * xx + yy * yy + zz * zz <= this.radius + radius * radius);
	}

}
