package game.rendering.debugscreen;

import org.lwjgl.input.Keyboard;

import engine.game.IGameLogic;
import engine.inputs.Input;
import engine.rendering.Renderer;
import engine.rendering.guis.GuiLabel;
import engine.rendering.guis.manager.GuiManager;

public class DebugScreen implements IGameLogic
{

	private float width, height;

	private boolean debuged;

	private GuiManager guiManager;
	private GuiLabel fpsLabel;

	public DebugScreen(float width, float height)
	{
		this.width = width;
		this.height = height;
		this.debuged = false;
		this.guiManager = new GuiManager();
		this.fpsLabel = new GuiLabel(0, 0, 100, 25, "FPS", 16);
	}

	@Override
	public void init()
	{
		GuiManager.addLabel(fpsLabel);
	}

	@Override
	public void render()
	{
		if (debuged)
		{
			Renderer.ortho();
			
			guiManager.render();
		}
	}

	@Override
	public void update()
	{
		if (Input.getKeyDown(Keyboard.KEY_F3))
		{
			if (!debuged)
			{
				debuged = true;
			} else
			{
				debuged = false;
			}
		}
	}

	@Override
	public void cleanUp()
	{

	}

	public void setDebuged(boolean debuged)
	{
		this.debuged = debuged;
	}

	public float getWidth()
	{
		return width;
	}

	public void setWidth(float width)
	{
		this.width = width;
	}

	public float getHeight()
	{
		return height;
	}

	public void setHeight(float height)
	{
		this.height = height;
	}

}
