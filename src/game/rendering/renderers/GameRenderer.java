package game.rendering.renderers;

import org.joml.Vector3f;

import engine.game.IGameLogic;
import engine.rendering.shaders.Shader;
import game.entities.manager.EntityManager;
import game.entities.player.Player;
import game.world.WorldManager;
import game.world.skybox.Skybox;

public class GameRenderer implements IGameLogic
{
	
	private EntityManager entityManager;
	private Player player;
	
	private WorldManager worldManager;
	
	private Skybox skybox;
	
	public GameRenderer()
	{
		this.entityManager = new EntityManager();
		this.player = new Player(new Vector3f(16, 16, 16), 1, null, 1.0f);
		this.worldManager = new WorldManager(20);
	}

	@Override
	public void init()
	{
		player.setPerspectiveProjection(70.0f, 0.1f, 1000.0f);
		player.init();

		entityManager.add(player);
	
		skybox = new Skybox(new String[] { "sides.jpg", "sides.jpg", "top.jpg", "bottom.jpg", "sides.jpg", "sides.jpg" });
		
		worldManager.generateWorld();
		worldManager.createWorld();
	}

	@Override
	public void render()
	{
		entityManager.render();
		
		Shader.SKYBOX.bind();
		skybox.render(player.getPosition());
		Shader.SKYBOX.unbind();

		worldManager.render();
	}

	@Override
	public void update()
	{
		entityManager.update();
		
		worldManager.update(player);
	}

	@Override
	public void cleanUp()
	{
	}
	
	public WorldManager getWorldManager()
	{
		return worldManager;
	}
	
	public Player getPlayer()
	{
		return player;
	}

}
