package game.world.environment.trees;

import game.items.placeable.blocks.basics.OakLeafItem;
import game.items.placeable.blocks.basics.WoodItem;
import game.world.WorldManager;
import game.world.utils.WorldUtils;

public class Oak extends Tree
{

	@Override
	public void create(int x, int y, int z, WorldManager worldManager)
	{
		WorldUtils.makeSphere(worldManager, (float) x + 0.5f, (float) y + 7f, (float) z + 0.5f, 4, new OakLeafItem().getBlock());
		WorldUtils.makeCuboid(worldManager, x, y, z, x + 1, y + 8, z + 1, new WoodItem().getBlock());
	}

}
