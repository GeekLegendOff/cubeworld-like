package game.world.environment.trees;

import game.items.placeable.blocks.basics.BeechLeafItem;
import game.items.placeable.blocks.basics.WoodItem;
import game.world.WorldManager;
import game.world.utils.WorldUtils;

public class Beech extends Tree
{

	/*
	 * The tree trunck height (T) is between 10 and 14.
	 * The tree branch height is T - (random between 4 and 6).
	 * For each side there is a chance in two that the tree has a branch.
	 * Each branch has a lenght between 3 and 5.
	 * The top of the tree branches can be up to two blocks below the beginning of the trunk leaf
	 *
	 */
	
	@Override
	public void create(int x, int y, int z, WorldManager worldManager)
	{
		int trunckHeight = worldManager.getWorldNoise().getRandom().nextInt(5) + 13;
		WorldUtils.makeRoundedCuboid(worldManager, x - 3, y + 10, z - 3, x + 6, y + 18, z + 6, new BeechLeafItem().getBlock());
		WorldUtils.makeCuboid(worldManager, x, y, z, x + 3, y + trunckHeight, z + 3, new WoodItem().getBlock());
		int backBranchAltitude = trunckHeight - worldManager.getWorldNoise().getRandom().nextInt(3) - 9;
		int frontBranchAltitude = trunckHeight - worldManager.getWorldNoise().getRandom().nextInt(3) - 9;
		int leftBranchAltitude = trunckHeight - worldManager.getWorldNoise().getRandom().nextInt(3) - 9;
		int rightBranchAltitude = trunckHeight - worldManager.getWorldNoise().getRandom().nextInt(3) - 9;
		boolean backBranchExist = worldManager.getWorldNoise().getRandom().nextBoolean();
		boolean frontBranchExist = worldManager.getWorldNoise().getRandom().nextBoolean();
		boolean leftBranchExist = worldManager.getWorldNoise().getRandom().nextBoolean();
		boolean rightBranchExist = worldManager.getWorldNoise().getRandom().nextBoolean();
		int backBranchLenght = worldManager.getWorldNoise().getRandom().nextInt(3) + 3;
		int frontBranchLenght = worldManager.getWorldNoise().getRandom().nextInt(3) + 3;
		int leftBranchLenght = worldManager.getWorldNoise().getRandom().nextInt(3) + 3;
		int rightBranchLenght = worldManager.getWorldNoise().getRandom().nextInt(3) + 3;
		int backBranchHeight = trunckHeight - backBranchAltitude - worldManager.getWorldNoise().getRandom().nextInt(3) - 5;
		int frontBranchHeight = trunckHeight - backBranchAltitude - worldManager.getWorldNoise().getRandom().nextInt(3) - 5;
		int leftBranchHeight = trunckHeight - backBranchAltitude - worldManager.getWorldNoise().getRandom().nextInt(3) - 5;
		int rightBranchHeight = trunckHeight - backBranchAltitude - worldManager.getWorldNoise().getRandom().nextInt(3) - 5;
		
		if(backBranchExist) {
			WorldUtils.makeCuboid(worldManager,
					x + 1,
					y + backBranchAltitude,
					z + 3,
					x + 2,
					y + backBranchAltitude + 1,
					z + 3 + backBranchLenght,
					new WoodItem().getBlock());
			WorldUtils.makeCuboid(worldManager,
					x + 1,
					y + backBranchAltitude + 1,
					z + 2 + backBranchLenght,
					x + 2,
					y + backBranchAltitude + backBranchHeight,
					z + 3 + backBranchLenght,
					new WoodItem().getBlock());
			WorldUtils.makeRoundedCuboid(worldManager,
					x + 1 - 4,
					y + backBranchAltitude + backBranchHeight,
					z + 2 + backBranchLenght - 3 - worldManager.getWorldNoise().getRandom().nextInt(2),
					x + 2 + 4,
					y + backBranchAltitude + backBranchHeight + 4,
					z + 3 + backBranchLenght + 3 + worldManager.getWorldNoise().getRandom().nextInt(2),
					new BeechLeafItem().getBlock());
		}
		
		if(frontBranchExist) {
			WorldUtils.makeCuboid(worldManager,
					x + 1,
					y + frontBranchAltitude,
					z,
					x + 2,
					y + frontBranchAltitude + 1,
					z - frontBranchLenght,
					new WoodItem().getBlock());
			WorldUtils.makeCuboid(worldManager,
					x + 1,
					y + frontBranchAltitude + 1,
					z + 1 - frontBranchLenght,
					x + 2,
					y + frontBranchAltitude + frontBranchHeight,
					z - frontBranchLenght,
					new WoodItem().getBlock());
			WorldUtils.makeRoundedCuboid(worldManager,
					x + 1 - 4,
					y + frontBranchAltitude + frontBranchHeight,
					z - frontBranchLenght + 3 + worldManager.getWorldNoise().getRandom().nextInt(2),
					x + 2 + 4,
					y + frontBranchAltitude + frontBranchHeight + 4,
					z - frontBranchLenght - 3 - worldManager.getWorldNoise().getRandom().nextInt(2),
					new BeechLeafItem().getBlock());
		}
		
		if(leftBranchExist) {
			WorldUtils.makeCuboid(worldManager,
					x + 3,
					y + leftBranchAltitude,
					z + 1,
					x + 3 + leftBranchLenght,
					y + leftBranchAltitude + 1,
					z + 2,
					new WoodItem().getBlock());
			WorldUtils.makeCuboid(worldManager,
					x + 2 + leftBranchLenght,
					y + leftBranchAltitude + 1,
					z + 1,
					x + 3 + leftBranchLenght,
					y + leftBranchAltitude + leftBranchHeight,
					z + 2,
					new WoodItem().getBlock());
			WorldUtils.makeRoundedCuboid(worldManager,
					x + 2 + leftBranchLenght - 3 - worldManager.getWorldNoise().getRandom().nextInt(2),
					y + leftBranchAltitude + leftBranchHeight,
					z + 1 - 4,
					x + 3 + leftBranchLenght + 3 + worldManager.getWorldNoise().getRandom().nextInt(2),
					y + leftBranchAltitude + leftBranchHeight + 4,
					z + 2 + 4,
					new BeechLeafItem().getBlock());
		}
		
		if(rightBranchExist) {
			WorldUtils.makeCuboid(worldManager,
					x,
					y + rightBranchAltitude,
					z + 1,
					x - rightBranchLenght,
					y + rightBranchAltitude + 1,
					z + 2,
					new WoodItem().getBlock());
			WorldUtils.makeCuboid(worldManager,
					x + 1 - rightBranchLenght,
					y + rightBranchAltitude + 1,
					z + 1,
					x - rightBranchLenght,
					y + rightBranchAltitude + rightBranchHeight,
					z + 2,
					new WoodItem().getBlock());
			WorldUtils.makeRoundedCuboid(worldManager,
					x - rightBranchLenght + 3 + worldManager.getWorldNoise().getRandom().nextInt(2),
					y + rightBranchAltitude + rightBranchHeight,
					z + 1 - 4,
					x - rightBranchLenght - 3 - worldManager.getWorldNoise().getRandom().nextInt(2),
					y + rightBranchAltitude + rightBranchHeight + 4,
					z + 2 + 4,
					new BeechLeafItem().getBlock());
		}
	}

}
