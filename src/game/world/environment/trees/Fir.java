package game.world.environment.trees;

import engine.maths.Mathf;
import game.items.placeable.blocks.basics.RifLeafItem;
import game.items.placeable.blocks.basics.WoodItem;
import game.world.WorldManager;
import game.world.utils.WorldUtils;

public class Fir extends Tree
{

	@Override
	public void create(int x, int y, int z, WorldManager worldManager)
	{
		for (int a = 0; a < 10; a++)
		{
			for (int b = 0; b < 11; b++)
			{
				for (int c = 0; c < 10; c++)
				{
					float a2 = a - 5.0f;
					float b2 = b - 5.0f;
					float c2 = c - 5.0f;
					float size = 1.0f;
					float l = Mathf.sqrt(a2 * a2 + c2 * c2);
					
					size -= b / 10.0f;
					
					if (l < 5.0f * size)
					{
						WorldUtils.setBlock(worldManager, (int) (x + a2), (int) (y + b2 + 9.0f), (int) (z + c2), new RifLeafItem().getBlock());
					}
					
				}
			}
		}
		WorldUtils.makeCuboid(worldManager, x, y, z, x + 1, y + 10, z + 1, new WoodItem().getBlock());

	}

}
