package game.world.environment.trees;

import game.items.placeable.blocks.basics.AppleLeafItem;
import game.items.placeable.blocks.basics.AppleTreeLeafItem;
import game.items.placeable.blocks.basics.WoodItem;
import game.world.WorldManager;
import game.world.utils.WorldUtils;

public class AppleTree extends Tree
{

	@Override
	public void create(int x, int y, int z, WorldManager worldManager)
	{
		
		int trunckHeight = worldManager.getWorldNoise().getRandom().nextInt(4) + 7;
		
		WorldUtils.makeRoundedCuboid(worldManager, x - 4, y + trunckHeight - 3, z - 4, x + 5, y + trunckHeight + 3, z + 5, new AppleTreeLeafItem().getBlock());
		WorldUtils.replace(worldManager, x - 4, y + trunckHeight - 3, z - 4, x + 5, y + trunckHeight + 3, z + 5, new AppleTreeLeafItem().getBlock(), new AppleLeafItem().getBlock(), 3);
		WorldUtils.makeCuboid(worldManager, x, y, z, x + 1, y + trunckHeight, z + 1, new WoodItem().getBlock());
	}
	

}
