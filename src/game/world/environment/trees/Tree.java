package game.world.environment.trees;

import java.util.ArrayList;
import java.util.List;

import game.world.WorldManager;

public abstract class Tree
{
	
	private static List<Tree> trees = new ArrayList<>();
	
	public static void registerTrees()
	{
		trees.add(new Oak());
		trees.add(new Fir());
		trees.add(new Beech());
		trees.add(new AppleTree());
	}
	
	public static void addTree(int x, int y, int z, WorldManager worldManager)
	{
		int treeType = worldManager.getWorldNoise().getRandom().nextInt(trees.size());
		trees.get(treeType).create(x, y, z, worldManager);
	}
	
	public abstract void create(int x, int y, int z, WorldManager worldManager);
	
	
	
}