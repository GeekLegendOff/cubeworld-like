package game.world.environment.structures;

import org.joml.Vector3f;

import engine.rendering.model.OBJModel;

public abstract class Structure
{

	public Vector3f position;
	public OBJModel objModel;
	public float size;
	
	public Structure(Vector3f position, OBJModel objModel, float size)
	{
		this.position = position;
		this.objModel = objModel;
		this.size = size;
	}
	
	public abstract void render();
	
	public abstract void cleanUp();
	
	public Vector3f getPosition()
	{
		return position;
	}
	
	public void setPosition(Vector3f position)
	{
		this.position = position;
	}
	
	public OBJModel getObjModel()
	{
		return objModel;
	}
	
	public float getSize()
	{
		return size;
	}
	
	public void setSize(float size)
	{
		this.size = size;
	}
	
}
