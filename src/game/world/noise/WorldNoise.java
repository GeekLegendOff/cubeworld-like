package game.world.noise;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import game.world.chunks.Chunk;

public class WorldNoise
{

	public static final long SEED = 15641L;
	public static final int OCTAVE = 16;
	public static final int AMPLITUDE = 5;
	private Noise noise;
	private Random random;
	private float[][] noises;
	private List<Noise> noisePasses;

	public WorldNoise(int size)
	{
		this.random = new Random(15641L);

		this.noises = new float[size * Chunk.SIZE][size * Chunk.SIZE];
		this.noisePasses = new ArrayList<Noise>();

		this.noisePasses.add(new Noise(15641L, 16, 5.0F, 0.0F, 1.0F));
		this.noisePasses.add(new Noise(15641L, 80, 20.0F, 0.0F, 1.0F));

		for (int xx = 0; xx < size; xx++)
		{

			for (int zz = 0; zz < size; zz++)
			{

				int xi = xx * Chunk.getSize();
				int zi = zz * Chunk.getSize();

				for (int x = 0; x < Chunk.SIZE; x++)
				{
					for (int z = 0; z < Chunk.SIZE; z++)
					{
						float n = -10000.0F;

						for (Noise p : noisePasses)
						{

							float nn = p.getNoise((x + xi), (z + zi));
							if (nn > n)
								n = nn;
						}
						this.noises[x + xi][z + zi] = n + 8.0F;
					}
				}
			}
		}
	}

	public float getHeight(int x, int z)
	{
		return this.noises[x][z];
	}

	public Random getRandom()
	{
		return this.random;
	}

	public Noise getNoise()
	{
		return noise;
	}

}
