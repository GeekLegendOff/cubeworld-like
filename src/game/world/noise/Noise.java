package game.world.noise;

import java.util.Random;

import org.joml.Vector2f;

public class Noise
{

	private long seed;
	private float amplitude;
	private int octave;
	private Random rand;
	private float height;
	private float inverse;

	public Noise(long seed, int octave, float amplitude, float height, float inverse)
	{
		this.seed = seed;
		this.octave = octave;
		this.amplitude = amplitude;
		this.height = height;
		this.inverse = inverse;

		this.rand = new Random();
	}

	public float getNoise(float x, float z)
	{
		int xmin = (int) x / this.octave;
		int xmax = xmin + 1;
		int zmin = (int) z / this.octave;
		int zmax = zmin + 1;

		Vector2f a = new Vector2f(xmin, zmin);
		Vector2f b = new Vector2f(xmax, zmin);
		Vector2f c = new Vector2f(xmax, zmax);
		Vector2f d = new Vector2f(xmin, zmax);

		float ra = (float) noise(a);
		float rb = (float) noise(b);
		float rc = (float) noise(c);
		float rd = (float) noise(d);

		float t1 = (x - (xmin * this.octave)) / this.octave;
		float t2 = (z - (zmin * this.octave)) / this.octave;

		float i1 = interpolate(ra, rb, t1);
		float i2 = interpolate(rd, rc, t1);
		float h = interpolate(i1, i2, t2);

		return ((h * 2.0F - 1.0F) * this.inverse + this.height) * this.amplitude;
	}

	private float interpolate(float a, float b, float t)
	{
		float ft = (float) (t * Math.PI);
		float f = (float) ((1.0D - Math.cos(ft)) * 0.5D);
		return a * (1.0F - f) + b * f;
	}

	private double noise(Vector2f coord)
	{
		double var = 10000.0D * (Math.sin(coord.x + Math.cos(coord.y)) + Math.tan(this.seed));
		this.rand.setSeed((long) var);
		return this.rand.nextDouble();
	}

}
