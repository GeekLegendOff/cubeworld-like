package game.world.skybox;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDepthMask;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.imageio.ImageIO;

import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import game.Game;

public class Skybox
{

	private int vbo;
	private int bufferSize;
	private FloatBuffer skyboxBuffer;
	private int textureID;

	public Skybox(String[] textures)
	{
		create(textures);
		createBuffers();
	}

	public void render(Vector3f position)
	{
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glDepthMask(false);

		glPushMatrix();

		glTranslatef(position.x, position.y + Game.getInstance().getGameRenderer().getPlayer().getHeight(), position.z);

		glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glVertexAttribPointer(0, 3, GL_FLOAT, false, 3 * 4, 0);

		glDrawArrays(GL_QUADS, 0, bufferSize);

		glDisableVertexAttribArray(0);

		glPopMatrix();

		glDepthMask(true);

		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
	}

	private void create(String[] textures)
	{
		skyboxBuffer = BufferUtils.createFloatBuffer(6 * 4 * 3);
		skyboxBuffer.put(getData()).flip();

		bufferSize += 6 * 4;

		textureID = glGenTextures();

		glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

		for (int i = 0; i < textures.length; i++)
		{
			TextureData textureData = decode(textures[i]);

			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, textureData.getWidth(),
					textureData.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData.getBuffer());
		}
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	private void createBuffers()
	{
		vbo = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, skyboxBuffer, GL_STATIC_DRAW);
	}

	private TextureData decode(String path)
	{
		int[] pixels = null;
		int width = 0;
		int height = 0;
		try
		{
			BufferedImage image = ImageIO.read(new File("ressources/textures/skybox/" + path));
			width = image.getWidth();
			height = image.getHeight();
			pixels = new int[width * height];
			image.getRGB(0, 0, width, height, pixels, 0, width);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		int[] data = new int[pixels.length];
		for (int i = 0; i < pixels.length; i++)
		{
			int a = (pixels[i] & 0xff000000) >> 24;
			int r = (pixels[i] & 0xff0000) >> 16;
			int g = (pixels[i] & 0xff00) >> 8;
			int b = (pixels[i] & 0xff);

			data[i] = a << 24 | b << 16 | g << 8 | r;
		}
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();

		return new TextureData(glGenTextures(), width, height, buffer);
	}

	private float[] getData()
	{
		int size = 1;

		return new float[]
		{ -size, -size, -size, -size, size, -size, size, size, -size, size, -size, -size, -size, -size, size, -size,
				size, size, size, size, size, size, -size, size, size, -size, -size, size, size, -size, size, size,
				size, size, -size, size, -size, -size, -size, -size, size, -size, -size, size, size, -size, -size, size,
				-size, size, -size, -size, size, size, size, size, size, size, size, -size, -size, -size, -size, -size,
				-size, size, size, -size, size, size, -size, -size, };
	}

	public float[] getTextureData()
	{
		int size = 1;

		return new float[]
		{ -size, -size, size, -size, size, size, -size, size, };
	}

	class TextureData
	{
		private int id;
		private int width;
		private int height;
		private IntBuffer buffer;

		public TextureData(int id, int width, int height, IntBuffer buffer)
		{
			this.id = id;
			this.width = width;
			this.height = height;
			this.buffer = buffer;
		}

		public int getId()
		{
			return id;
		}

		public int getWidth()
		{
			return width;
		}

		public int getHeight()
		{
			return height;
		}

		public IntBuffer getBuffer()
		{
			return buffer;
		}
	}

}
