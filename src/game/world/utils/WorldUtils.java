package game.world.utils;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector3i;

import game.Game;
import game.items.placeable.blocks.Block;
import game.world.WorldManager;

public class WorldUtils
{
	
	public static void setBlock(WorldManager wm, int x, int y, int z, Block block)
	{
		wm.setBlock(x, y, z, block);
	}
	
	public static void setBlock(int x, int y, int z, Block block)
	{
		setBlock(getWorldManager(), x, y, z, block);
	}
	
	public static void setBlock(WorldManager wm, Vector3i pos, Block block)
	{
		setBlock(wm, pos.x, pos.y, pos.z, block);
	}
	
	public static void setBlock(Vector3i pos, Block block)
	{
		setBlock(getWorldManager(), pos, block);
	}
	
	public static void makeCuboid(WorldManager wm, int x, int y, int z, int _x, int _y, int _z, Block block)
	{
		if(Math.max(x, _x) == x)
		{
			int __x = x;
			x = _x;
			_x = __x;
		}
		if(Math.max(y, _y) == y)
		{
			int __y = y;
			y = _y;
			_y = __y;
		}
		if(Math.max(z, _z) == z)
		{
			int __z = z;
			z = _z;
			_z = __z;
		}
		
		// _a > a
		int widht = _x - x;
		int height = _y - y;
		int lenght = _z - z;
		
		for (int i = 0; i < widht; i++)
		{
			for (int j = 0; j < height; j++)
			{
				for (int k = 0; k < lenght; k++)
				{
					setBlock(x + i, y + j, z + k, block);
				}
			}
		}
		
	}
	
	public static void makeCuboid(int x, int y, int z, int _x, int _y, int _z, Block block)
	{
		makeCuboid(getWorldManager(), x, y, z, _x, _y, _z, block);
	}
	
	public static void makeCuboid(WorldManager wm, Vector3i pos, Vector3i _pos, Block block)
	{
		makeCuboid(wm, pos.x, pos.y, pos.z, _pos.x, _pos.y, _pos.z, block);
	}
	
	public static void makeCuboid(Vector3i pos, Vector3i _pos, Block block)
	{
		makeCuboid(getWorldManager(), pos.x, pos.y, pos.z, _pos.x, _pos.y, _pos.z, block);
	}
	
	public static void makeRoundedCuboid(WorldManager wm, int x, int y, int z, int _x, int _y, int _z, Block block)
	{
		if(Math.max(x, _x) == x)
		{
			int __x = x;
			x = _x;
			_x = __x;
		}
		if(Math.max(y, _y) == y)
		{
			int __y = y;
			y = _y;
			_y = __y;
		}
		if(Math.max(z, _z) == z)
		{
			int __z = z;
			z = _z;
			_z = __z;
		}
		makeCuboid(wm, x + 1, y + 1, z + 1, _x - 1, _y - 1, _z - 1, block);
		makeCuboid(wm, x + 2, y, z + 2, _x - 2, y + 1, _z - 2, block);
		makeCuboid(wm, x + 2, _y, z + 2, _x - 2, _y - 1, _z - 2, block);
		makeCuboid(wm, x, y + 2, z + 2, x + 1, _y - 2, _z - 2, block);
		makeCuboid(wm, _x, y + 2, z + 2, _x - 1, _y - 2, _z - 2, block);
		makeCuboid(wm, x + 2, y + 2, z, _x - 2, _y - 2, z + 1, block);
		makeCuboid(wm, x + 2, y + 2, _z, _x - 2, _y - 2, _z - 1, block);
	}
	
	public static void makeRoundedCuboid(int x, int y, int z, int _x, int _y, int _z, Block block)
	{
		makeRoundedCuboid(getWorldManager(), x, y, z, _x, _y, _z, block);
	}
	
	public static void makeRoundedCuboid(WorldManager wm, Vector3i pos, Vector3i _pos, Block block)
	{
		makeRoundedCuboid(wm, pos.x, pos.y, pos.z, _pos.x, _pos.y, _pos.z, block);
	}
	
	public static void makeRoundedCuboid(Vector3i pos, Vector3i _pos, Block block)
	{
		makeRoundedCuboid(getWorldManager(), pos.x, pos.y, pos.z, _pos.x, _pos.y, _pos.z, block);
	}
	
	public static void makeSphere(WorldManager wm, Vector3f center, int r, Block block)
	{
		for (int i = (0 - r); i <= r; i++)
		{
			for (int j = (0 - r); j <= r; j++)
			{
				for (int k = (0 - r); k <= r; k++)
				{
					Vector3f p = new Vector3f(i, j, k);
					p.add(center);
					if(center.distance(p) < r) {
						setBlock(wm, (int) p.x, (int) p.y, (int) p.z, block);
					}
				}
			}
		}
	}
	
	public static void makeSphere(Vector3f center, int r, Block block)
	{
		makeSphere(getWorldManager(), center, r, block);
	}
	
	public static void makeSphere(WorldManager wm, float center_x, float center_y, float center_z, int r, Block block)
	{
		makeSphere(wm, new Vector3f(center_x, center_y, center_z), r, block);
	}
	
	public static void makeSphere(float center_x, float center_y, float center_z, int r, Block block)
	{
		makeSphere(getWorldManager(), new Vector3f(center_x, center_y, center_z), r, block);
	}
	
	public static void makeSegment(WorldManager wm, int x, int y, int z, int _x, int _y, int _z, Block block)
	{
		setBlock(wm, x, y, z, block);
        int deltaX = Math.abs(_x-x);
        int deltaY = Math.abs(_y-y);
        int deltaZ = Math.abs(_z-z);

        int xs = (_x > x)?1:-1;
        int ys = (_y > y)?1:-1;
        int zs = (_z > z)?1:-1;
        if(deltaX >= deltaY && deltaX >= deltaZ) {
            int p1 = 2 * deltaY - deltaX;
            int p2 = 2 * deltaZ - deltaX;
            while (x != _x) {
                x += xs;
                if (p1 >= 0) {
                    y += ys;
                    p1 -= 2 * deltaX;
                }
                if (p2 >= 0) {
                    z += zs;
                    p2 -= 2 * deltaX;
                }
                p1 += 2 * deltaY;
                p2 += 2 * deltaZ;
                setBlock(wm, x, y, z, block);
            }
        }
        else if(deltaY >= deltaX && deltaY >= deltaZ) {
            int p1 = 2 * deltaX - deltaY;
            int p2 = 2 * deltaZ - deltaY;
            while (y != _y) {
                y += ys;
                if (p1 >= 0) {
                    x += xs;
                    p1 -= 2* deltaY;
                }
                if (p2 >= 0) {
                    z += zs;
                    p2 -= 2* deltaY;
                }
                p1 += 2 * deltaX;
                p2 += 2 * deltaZ;
                setBlock(wm, x, y, z, block);
            }
        }
        else  {
            int p1 = 2 * deltaY - deltaZ;
            int p2 = 2 * deltaX - deltaZ;
            while (z != _z) {
                z += zs;
                if (p1 >= 0) {
                    y += ys;
                    p1 -= 2 * deltaZ;
                }
                if (p2 >= 0) {
                    x += xs;
                    p2 -= 2 * deltaZ;
                }
                p1 += 2 * deltaY;
                p2 += 2 * deltaX;
                setBlock(wm, x, y, z, block);
            }
        }
	}
	
	public static void makeSegment(int x, int y, int z, int _x, int _y, int _z, Block block)
	{
		makeSegment(getWorldManager(), x, y, z, _x, _y, _z, block);
	}
	
	public static void makeSegment(WorldManager wm, Vector3i pos, Vector3i _pos, Block block)
	{
		makeSegment(wm, pos.x, pos.y, pos.z, _pos.x, _pos.y, _pos.z, block);
	}
	
	public static void makeSegment(Vector3i pos, Vector3i _pos, Block block)
	{
		makeSegment(getWorldManager(), pos.x, pos.y, pos.z, _pos.x, _pos.y, _pos.z, block);
	}
	
	public static void makeDisk(WorldManager wm, float center_x, int center_y, float center_z, int r, Block block)
	{
		Vector2f center = new Vector2f(center_x, center_z);
		for (int i = (0 - r); i <= r; i++)
		{
			for (int j = (0 - r); j <= r; j++)
			{
				Vector2f p = new Vector2f(i, j);
				p.add(center);
				if(center.distance(p) < r) {
					setBlock(wm, (int) p.x, center_y, (int) p.y, block);
				}
			}
		}
	}
	
	public static void makeDisk(float center_x, int center_y, float center_z, int r, Block block)
	{
		makeDisk(getWorldManager(), center_x, center_y, center_z, r, block);
	}
	
	public static void makeDisk(WorldManager wm, Vector3f center, int r, Block block)
	{
		makeDisk(wm, center.x, (int) center.y, center.z, r, block);
	}
	
	public static void makeDisk(Vector3f center, int r, Block block)
	{
		makeDisk(getWorldManager(), center, r, block);	
	}
	
	public static void makeCylinder(WorldManager wm, float center_x, int center_y, float center_z, int r, int height, Block block)
	{
		for (int i = 0; i < height; i++)
		{
			makeDisk(wm, center_x, i + center_y, center_z, r, block);
		}
	}
	
	public static void makeCylinder(float center_x, int center_y, float center_z, int r, int height, Block block)
	{
		makeCylinder(getWorldManager(), center_x, center_y, center_z, r, height, block);
	}
	
	public static void makeCylinder(WorldManager wm, Vector3f pos, int r, int height, Block block)
	{
		makeCylinder(wm, pos.x, (int) pos.y, pos.z, r, height, block);
	}
	
	public static void makeCylinder(Vector3f pos, int r, int height, Block block)
	{
		makeCylinder(getWorldManager(), pos, r, height, block);
	}
	
	public static void makeCone(WorldManager wm, float center_x, int center_y, float center_z, int height, Block block)
	{
		for (int i = 0; i < height; i++)
		{
			makeDisk(wm, center_x, center_y + i, center_z, height - i, block);
		}
	}
	
	public static void replace(WorldManager wm, int x, int y, int z, int _x, int _y, int _z, Block block, Block replace, int percentage)
	{
		if(Math.max(x, _x) == x)
		{
			int __x = x;
			x = _x;
			_x = __x;
		}
		if(Math.max(y, _y) == y)
		{
			int __y = y;
			y = _y;
			_y = __y;
		}
		if(Math.max(z, _z) == z)
		{
			int __z = z;
			z = _z;
			_z = __z;
		}
		for (int i = x; i < _x; i++)
		{
			for (int j = y; j < _y; j++)
			{
				for (int k = z; k < _z; k++)
				{
					if((wm.getWorldNoise().getRandom().nextInt(100) + 1) <= percentage && wm.getBlockId(i, j, k) == block.getId())
					{
						setBlock(wm, i, j, k, replace);
					}
				}
			}
		}
		
	}
	
	public static void replace(int x, int y, int z, int _x, int _y, int _z, Block block, Block replace, int percentage)
	{
		replace(getWorldManager(), x, y, z, _x, _y, _z, block, replace, percentage);
	}

	public static void replace(WorldManager wm, Vector3i pos, Vector3i _pos, Block block, Block replace, int percentage)
	{
		replace(wm, pos.x, pos.y, pos.z, _pos.x, _pos.y, _pos.z, block, replace, percentage);
	}
	
	public static void replace(Vector3i pos, Vector3i _pos, Block block, Block replace, int percentage)
	{
		replace(getWorldManager(), pos, _pos, block, replace, percentage);
	}
		
	public static WorldManager getWorldManager()
	{
		return Game.getInstance().getGameRenderer().getWorldManager();
	}

}
