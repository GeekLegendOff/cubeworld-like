package game.world;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.util.Log;

import engine.maths.Indexer;
import game.entities.player.Player;
import game.items.ItemManager;
import game.items.placeable.blocks.Block;
import game.world.chunks.Chunk;
import game.world.chunks.ChunkRenderer;
import game.world.noise.WorldNoise;

public class WorldManager
{

	private ItemManager itemManager;
	private int size;
	private float gravity;
	private WorldNoise worldNoise;
	private Map<Integer, Chunk> chunks;
	private Map<Integer, ChunkRenderer> chunkRenderer;
	
	public WorldManager(int size)
	{
		itemManager = new ItemManager();
		itemManager.init();
		this.size = size;
		this.gravity = 1.0f;
		this.worldNoise = new WorldNoise(size);
		this.chunks = new HashMap<Integer, Chunk>();
		this.chunkRenderer = new HashMap<Integer, ChunkRenderer>();
	}
	
	public void generateWorld()
	{
		for (int x = 0; x < size; x++)
		{
			for (int z = 0; z < size; z++)
			{
				int index = Indexer.index3i(x, 0, z);
				
				Chunk chunk = new Chunk(x, 0, z, this);
				
				chunks.put(index, chunk);
			}
		}
		for (int x = 0; x < size; x++)
		{
			for (int z = 0; z < size; z++)
			{
				Log.info("x:"+x+" z: "+z);
				int index = Indexer.index3i(x, 0, z);
				
				Chunk chunk = chunks.get(index);
				chunk.generateVegetation();
			}
		}
	}
	
	public void createWorld()
	{
		if (chunkRenderer.size() != chunks.size())
		{
			for (Map.Entry<Integer, Chunk> entry : chunks.entrySet())
			{
				if (chunkRenderer.containsKey(entry.getKey()))
				{
					continue;
				}
				
				Chunk chunk = entry.getValue();
				ChunkRenderer cr = new ChunkRenderer(chunk, this);
				
				chunkRenderer.put(entry.getKey(), cr);
			}
		}
	}
	
	public void render()
	{
		for (ChunkRenderer cr : chunkRenderer.values())
		{
			cr.render();
		}
	}
	
	public void update(Player player)
	{
		if (chunkRenderer.size() != chunks.size())
		{
			for (Map.Entry<Integer, Chunk> entry : chunks.entrySet())
			{
				if (chunkRenderer.containsKey(entry.getKey()))
				{
					continue;
				}
				
				Chunk chunk = entry.getValue();
				ChunkRenderer cr = new ChunkRenderer(chunk, this);
				
				chunkRenderer.put(entry.getKey(), cr);
			}
		}
		
		for (ChunkRenderer cr : chunkRenderer.values())
		{
			cr.update(player);
		}
	}
	
	public void updateChunk(int x, int z)
	{
		Chunk chunk = getChunk(x, z);
		
		if (chunk == null)
		{
			return;
		}
		
		chunk.setUpdateRequest(true);
	}
	
	public WorldNoise getWorldNoise()
	{
		return worldNoise;
	}

	public Chunk getChunk(int x, int z)
	{
		int xc = x / Chunk.getSize();
		int zc = z / Chunk.getSize();
		
		if (xc < 0 || zc < 0 || xc >= Chunk.getSize() || zc >= Chunk.getSize())
		{
			return null;
		}
		return chunks.get(Indexer.index3i(xc, 0, zc));
	}
	
	public String getBlockId(int x, int y, int z)
	{
		Chunk chunk = getChunk(x, z);
		
		if (chunk == null)
		{
			return null;
		}
		
		int xb = x % Chunk.SIZE;  
		int yb = y % Chunk.HEIGHT;
		int zb = z % Chunk.SIZE;  
		
		return chunk.getBlockId(xb, yb, zb);
	}
	
	public Block getBlock(int x, int y, int z)
	{
		return Block.getBlockById(getBlockId(x, y, z));
	}
	
	public void setBlock(int x, int y, int z, String block)
	{
		Chunk chunk = getChunk(x, z);
		
		if (chunk == null)
		{
			return;
		}
		
		int xb = x % Chunk.SIZE;
		int yb = y % Chunk.HEIGHT;
		int zb = z % Chunk.SIZE;
		
		chunk.addBlock(xb, yb, zb, block);
	}
	
	public void setBlock(int x, int y, int z, Block block)
	{
		Chunk chunk = getChunk(x, z);
		
		if (chunk == null)
		{
			return;
		}
		
		int xb = x % Chunk.SIZE;  
		int yb = y % Chunk.HEIGHT;
		int zb = z % Chunk.SIZE;  
		
		chunk.addBlock(xb, yb, zb, block.getId());
	}
	
	public float getGravity()
	{
		return gravity;
	}

	public ItemManager getItemManager()
	{
		return itemManager;
	}
	
}
