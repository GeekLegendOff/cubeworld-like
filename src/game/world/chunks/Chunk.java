package game.world.chunks;

import org.joml.Vector3f;
import org.newdawn.slick.util.Log;

import game.items.placeable.blocks.Block;
import game.items.placeable.blocks.basics.GrassItem;
import game.world.WorldManager;
import game.world.environment.trees.Tree;

public class Chunk
{

	public static final int SIZE = 16;
	public static final int HEIGHT = 32;

	public String[][][] blocks;

	private WorldManager worldManager;

	public int x, y, z;

	private boolean updateRequest;

	private Vector3f centerPosition;
	
	public Chunk(int x, int y, int z, WorldManager worldManager)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.worldManager = worldManager;
		this.blocks = new String[SIZE][HEIGHT][SIZE];
		this.updateRequest = false;
		this.centerPosition = new Vector3f((x * SIZE + 8), 20.0f, (z * SIZE + 8));
		
		generateChunk();
	}
	
	public void generateChunk()
	{
		for (int x = 0; x < SIZE; x++)
		{
			for (int y = 0; y < HEIGHT; y++)
			{
				for (int z = 0; z < SIZE; z++)
				{
					int xx = this.x * SIZE + x;
					int yy = this.y * HEIGHT + y;
					int zz = this.z * SIZE + z;

					if(this.x > 15 && this.z > 15)
						Log.info("" + worldManager.getWorldNoise().getHeight(xx, zz) + " yy: "+yy);
					
					if (worldManager.getWorldNoise().getHeight(xx, zz) > yy - 1)
					{
						addBlock(x, y, z, new GrassItem().getBlock().getId());
					}
				}
			}
		}
	}

	public void generateVegetation()
	{
		for (int x = 0; x < SIZE; x++)
		{
			for (int y = 0; y < HEIGHT; y++)
			{
				for (int z = 0; z < SIZE; z++)
				{
					int xx = this.x * SIZE + x;
					int yy = this.y * HEIGHT + y;
					int zz = this.z * SIZE + z;

					boolean grounded = (worldManager.getWorldNoise().getHeight(xx, zz) > (yy - 1) && worldManager.getWorldNoise().getHeight(xx, zz) < yy);

					if (worldManager.getWorldNoise().getRandom().nextFloat() < 0.005f && grounded)
					{
						Tree.addTree(xx, yy, zz, worldManager);
					}
				}
			}
		}
	}

	public String getBlockId(int x, int y, int z)
	{
		if (x < 0 || y < 0 || z < 0 || x >= SIZE || y >= HEIGHT || z >= SIZE)
		{
			return null;
		}
		return blocks[x][y][z];
	}
	
	public Block getBlock(int x, int y, int z)
	{
		return Block.getBlockById(getBlockId(x, y, z));
	}

	public void addBlock(int x, int y, int z, String block)
	{
		if (x < 0 || y < 0 || z < 0 || x >= SIZE || y >= HEIGHT || z >= SIZE)
		{
			return;
		}
		blocks[x][y][z] = block;
	}

	public static int getSize()
	{
		return SIZE;
	}

	public static int getHeight()
	{
		return HEIGHT;
	}

	public boolean isUpdateRequest()
	{
		return updateRequest;
	}

	public void setUpdateRequest(boolean updateRequest)
	{
		this.updateRequest = updateRequest;
	}

	public Vector3f getCenterPosition()
	{
		return centerPosition;
	}

}
