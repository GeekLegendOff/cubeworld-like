package game.world.chunks;

import static org.lwjgl.opengl.GL11.GL_COLOR_ARRAY;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_VERTEX_ARRAY;
import static org.lwjgl.opengl.GL11.glColorPointer;
import static org.lwjgl.opengl.GL11.glDisableClientState;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glEnableClientState;
import static org.lwjgl.opengl.GL11.glVertexPointer;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

import game.entities.player.Player;
import game.items.placeable.blocks.Block;
import game.items.placeable.blocks.basics.GrassItem;
import game.world.WorldManager;

public class ChunkRenderer
{

	private Chunk chunk;

	private WorldManager worldManager;

	private int vbo, cbo;
	private int bufferSize;
	
	private FloatBuffer vertexBuffer, vertexColorBuffer;

	private boolean isInViewDistance;

	public ChunkRenderer(Chunk chunk, WorldManager worldManager)
	{
		this.chunk = chunk;
		this.worldManager = worldManager;
		
		createBuffer();
	}

	public void render()
	{
		if (!isInViewDistance)
		{
			return;
		}
		
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glVertexPointer(3, GL_FLOAT, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		
		glBindBuffer(GL_ARRAY_BUFFER, cbo);
		glColorPointer(4, GL_FLOAT, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);  
		
		glDrawArrays(GL_QUADS, 0, bufferSize);
		
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
	}

	public void update(Player player)
	{
		if (chunk.isUpdateRequest())
		{
			createBuffer();

			chunk.setUpdateRequest(false);
		}

		isInViewDistance = player.getDistanceCulling().isInViewDistance(chunk.getCenterPosition(), 160.0f);
	}

	private void createBuffer()
	{
		if (vertexBuffer == null)
		{
			vertexBuffer = BufferUtils
					.createFloatBuffer(Chunk.SIZE * Chunk.HEIGHT * Chunk.SIZE * 6 * 4 * 3);
		}

		if (vertexColorBuffer == null)
		{
			vertexColorBuffer = BufferUtils
					.createFloatBuffer(Chunk.SIZE * Chunk.HEIGHT * Chunk.SIZE * 6 * 4 * 4);
		}
		
		createChunk();
		
		vbo = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		
		cbo = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, cbo);
		glBufferData(GL_ARRAY_BUFFER, vertexColorBuffer, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		vertexBuffer.clear();
		vertexColorBuffer.clear();
	}

	private void createChunk()
	{
		bufferSize = 0;

		for (int x = 0; x < Chunk.SIZE; x++)
		{
			for (int y = 0; y < Chunk.HEIGHT; y++)
			{
				for (int z = 0; z < Chunk.SIZE; z++)
				{
					Block block = chunk.getBlock(x, y, z);
					if (block != null)
					{
						
						if(block.getId().equals("grass")) block = new GrassItem(x, z).getBlock();
						
						int xx = chunk.x * Chunk.SIZE + x;
						int yy = chunk.y * Chunk.HEIGHT + y;
						int zz = chunk.z * Chunk.SIZE + z;

						boolean front = worldManager.getBlock(xx, yy, zz - 1) == null;
						boolean back = worldManager.getBlock(xx, yy, zz + 1) == null;
						boolean right = worldManager.getBlock(xx + 1, yy, zz) == null;
						boolean left = worldManager.getBlock(xx - 1, yy, zz) == null;
						boolean up = worldManager.getBlock(xx, yy + 1, zz) == null;
						boolean down = worldManager.getBlock(xx, yy - 1, zz) == null;

						if (up || down || right || left || front || back)
						{

							int size = 0;
							float ao = 1.0f;

							if (up)
							{

								float[] shading =
								{ 1.0f, 1.0f, 1.0f, 1.0f };

								if (worldManager.getBlock(xx - 1, yy + 1, zz) != null)
								{

									shading[0] = shading[0] * ao;
									shading[3] = shading[3] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy + 1, zz - 1) != null)
								{
									shading[0] = shading[0] * ao;
								}
								if (worldManager.getBlock(xx, yy + 1, zz - 1) != null)
								{

									shading[0] = shading[0] * ao;
									shading[1] = shading[1] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy + 1, zz) != null)
								{

									shading[1] = shading[1] * ao;
									shading[2] = shading[2] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy + 1, zz - 1) != null)
								{
									shading[1] = shading[1] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy + 1, zz + 1) != null)
								{
									shading[2] = shading[2] * ao;
								}
								if (worldManager.getBlock(xx, yy + 1, zz + 1) != null)
								{

									shading[2] = shading[2] * ao;
									shading[3] = shading[3] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy + 1, zz + 1) != null)
								{
									shading[3] = shading[3] * ao;
								}

								vertexBuffer.put(Block.getUpData(xx, yy, zz));
								vertexColorBuffer.put(Block.getUpColorData(block.getColor(), 1.0f, shading));
								size++;
							}
							if (down && yy != 0)
							{

								float[] shading =
								{ 1.0f, 1.0f, 1.0f, 1.0f };

								if (worldManager.getBlock(xx - 1, yy - 1, zz) != null)
								{

									shading[1] = shading[1] * ao;
									shading[2] = shading[2] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy - 1, zz - 1) != null)
								{
									shading[1] = shading[1] * ao;
								}
								if (worldManager.getBlock(xx, yy - 1, zz - 1) != null)
								{

									shading[1] = shading[1] * ao;
									shading[0] = shading[0] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy - 1, zz) != null)
								{

									shading[0] = shading[0] * ao;
									shading[3] = shading[3] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy - 1, zz - 1) != null)
								{
									shading[0] = shading[0] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy - 1, zz + 1) != null)
								{
									shading[3] = shading[3] * ao;
								}
								if (worldManager.getBlock(xx, yy - 1, zz + 1) != null)
								{

									shading[3] = shading[3] * ao;
									shading[2] = shading[2] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy - 1, zz + 1) != null)
								{
									shading[2] = shading[2] * ao;
								}

								vertexBuffer.put(Block.getDownData(xx, yy, zz));
								vertexColorBuffer.put(Block.getDownColorData(block.getColor(), 1.0f, shading));
								size++;
							}
							if (right && xx != 255)
							{

								float[] shading =
								{ 1.0f, 1.0f, 1.0f, 1.0f };

								if (worldManager.getBlock(xx + 1, yy - 1, zz) != null)
								{

									shading[1] = shading[1] * ao;
									shading[2] = shading[2] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy - 1, zz - 1) != null)
								{
									shading[1] = shading[1] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy, zz - 1) != null)
								{

									shading[1] = shading[1] * ao;
									shading[0] = shading[0] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy + 1, zz) != null)
								{

									shading[0] = shading[0] * ao;
									shading[3] = shading[3] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy + 1, zz - 1) != null)
								{
									shading[0] = shading[0] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy + 1, zz + 1) != null)
								{
									shading[3] = shading[3] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy, zz + 1) != null)
								{

									shading[3] = shading[3] * ao;
									shading[2] = shading[2] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy - 1, zz + 1) != null)
								{
									shading[2] = shading[2] * ao;
								}

								vertexBuffer.put(Block.getRightData(xx, yy, zz));
								vertexColorBuffer.put(Block.getRightColorData(block.getColor(), 1.0f, shading));
								size++;
							}
							if (left && xx != 0)
							{

								float[] shading =
								{ 1.0f, 1.0f, 1.0f, 1.0f };

								if (worldManager.getBlock(xx - 1, yy - 1, zz) != null)
								{

									shading[0] = shading[0] * ao;
									shading[3] = shading[3] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy - 1, zz - 1) != null)
								{
									shading[0] = shading[0] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy, zz - 1) != null)
								{

									shading[0] = shading[0] * ao;
									shading[1] = shading[1] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy + 1, zz) != null)
								{

									shading[1] = shading[1] * ao;
									shading[2] = shading[2] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy + 1, zz - 1) != null)
								{
									shading[1] = shading[1] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy + 1, zz + 1) != null)
								{
									shading[2] = shading[2] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy, zz + 1) != null)
								{

									shading[2] = shading[2] * ao;
									shading[3] = shading[3] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy - 1, zz + 1) != null)
								{
									shading[3] = shading[3] * ao;
								}

								vertexBuffer.put(Block.getLeftData(xx, yy, zz));
								vertexColorBuffer.put(Block.getLeftColorData(block.getColor(), 1.0f, shading));
								size++;
							}
							if (front && zz != 0)
							{

								float[] shading =
								{ 1.0f, 1.0f, 1.0f, 1.0f };

								if (worldManager.getBlock(xx, yy - 1, zz - 1) != null)
								{

									shading[0] = shading[0] * ao;
									shading[1] = shading[1] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy - 1, zz - 1) != null)
								{
									shading[0] = shading[0] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy, zz - 1) != null)
								{

									shading[0] = shading[0] * ao;
									shading[3] = shading[3] * ao;
								}

								if (worldManager.getBlock(xx + 1, yy - 1, zz - 1) != null)
								{
									shading[1] = shading[1] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy, zz - 1) != null)
								{

									shading[1] = shading[1] * ao;
									shading[2] = shading[2] * ao;
								}

								if (worldManager.getBlock(xx, yy + 1, zz - 1) != null)
								{

									shading[2] = shading[2] * ao;
									shading[3] = shading[3] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy + 1, zz - 1) != null)
								{
									shading[2] = shading[2] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy + 1, zz - 1) != null)
								{
									shading[3] = shading[3] * ao;
								}

								vertexBuffer.put(Block.getFrontData(xx, yy, zz));
								vertexColorBuffer.put(Block.getFrontColorData(block.getColor(), 1.0f, shading));
								size++;
							}
							if (back && zz != 255)
							{

								float[] shading =
								{ 1.0f, 1.0f, 1.0f, 1.0f };

								if (worldManager.getBlock(xx, yy - 1, zz + 1) != null)
								{

									shading[1] = shading[1] * ao;
									shading[0] = shading[0] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy - 1, zz + 1) != null)
								{
									shading[1] = shading[1] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy, zz + 1) != null)
								{

									shading[1] = shading[1] * ao;
									shading[2] = shading[2] * ao;
								}

								if (worldManager.getBlock(xx + 1, yy - 1, zz + 1) != null)
								{
									shading[0] = shading[0] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy, zz + 1) != null)
								{

									shading[0] = shading[0] * ao;
									shading[3] = shading[3] * ao;
								}

								if (worldManager.getBlock(xx, yy + 1, zz + 1) != null)
								{

									shading[3] = shading[3] * ao;
									shading[2] = shading[2] * ao;
								}
								if (worldManager.getBlock(xx + 1, yy + 1, zz + 1) != null)
								{
									shading[3] = shading[3] * ao;
								}
								if (worldManager.getBlock(xx - 1, yy + 1, zz + 1) != null)
								{
									shading[2] = shading[2] * ao;
								}

								vertexBuffer.put(Block.getBackData(xx, yy, zz));
								vertexColorBuffer.put(Block.getBackColorData(block.getColor(), 1.0f, shading));
								size++;
							}

							bufferSize += size * 4;
						}
					}
				}
			}
		}
		vertexBuffer.flip();
		vertexColorBuffer.flip();
	}

}
