package game.items.basics;

import game.items.Item;
import game.items.Rarity;

public class EasterEggItem extends Item
{

	public EasterEggItem()
	{
		super("easter_egg");
		setName("Easter Egg");
		setDescription("The game easter egg !");
		setStacksize(1);
		setRarity(Rarity.SPECIAL);
		setModel("models/item/easter_egg/easter_egg.obj");
	}

}
