package game.items.consumables.drink;

import game.items.consumables.ConsumableItem;

public abstract class DrinkItem extends ConsumableItem
{

	public DrinkItem(String id)
	{
		super(id);
	}

}
