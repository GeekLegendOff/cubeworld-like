package game.items.consumables.food.basics;

import game.items.consumables.food.FoodItem;

public class AppleItem extends FoodItem
{

	public AppleItem()
	{
		super("apple");
		setName("Apple");
		setDescription("For a balanced meal!");
		setFood(2);
		setSaturation(3600);
	}

}
