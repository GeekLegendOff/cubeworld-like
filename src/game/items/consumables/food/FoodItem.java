package game.items.consumables.food;

import game.items.consumables.ConsumableItem;

public abstract class FoodItem extends ConsumableItem
{
	
	private int food;
	private int saturation;

	public FoodItem(String id)
	{
		super(id);
	}

	public int getFood()
	{
		return food;
	}

	public void setFood(int food)
	{
		this.food = food;
	}

	public int getSaturation()
	{
		return saturation;
	}

	public void setSaturation(int saturation)
	{
		this.saturation = saturation;
	}

}
