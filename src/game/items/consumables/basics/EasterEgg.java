package game.items.consumables.basics;

import game.items.Item;
import game.items.Rarity;

public class EasterEgg extends Item
{

	public EasterEgg()
	{
		super("easter_egg");
		setName("Easter Egg");
		setRarity(Rarity.EPIC);
		setDescription("Only an Easter Egg !");
		setStacksize(1);
		//TODO setModel("TODO");
	}

	@Override
	public void update()
	{

	}

}
