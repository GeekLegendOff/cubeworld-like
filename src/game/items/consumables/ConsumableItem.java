package game.items.consumables;

import game.items.Item;

public abstract class ConsumableItem extends Item
{
	
	int consumptionTime = 0;

	public ConsumableItem(String id)
	{
		super(id);
	}

}
