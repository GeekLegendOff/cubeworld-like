package game.items;

import java.util.ArrayList;
import java.util.List;

import engine.game.IGameLogic;
import game.items.consumables.basics.EasterEgg;
import game.items.consumables.food.basics.AppleItem;
import game.items.placeable.blocks.basics.AppleLeafItem;
import game.items.placeable.blocks.basics.AppleTreeLeafItem;
import game.items.placeable.blocks.basics.BeechLeafItem;
import game.items.placeable.blocks.basics.CloudItem;
import game.items.placeable.blocks.basics.GrassItem;
import game.items.placeable.blocks.basics.OakLeafItem;
import game.items.placeable.blocks.basics.RifLeafItem;
import game.items.placeable.blocks.basics.WaterItem;
import game.items.placeable.blocks.basics.WoodItem;

public class ItemManager implements IGameLogic
{
	
	List<Item> items = new ArrayList<>();
	
	public ItemManager()
	{
		
	}
	
	private void registerItems()
	{
		addItem(new EasterEgg());
		addItem(new BeechLeafItem());
		addItem(new CloudItem());
		addItem(new GrassItem());
		addItem(new OakLeafItem());
		addItem(new RifLeafItem());
		addItem(new WaterItem());
		addItem(new WoodItem());
		addItem(new AppleTreeLeafItem());
		addItem(new AppleLeafItem());
		addItem(new AppleItem());
	}
	
	private void addItem(Item item)
	{
		items.add(item);
	}

	@Override
	public void init()
	{
		registerItems();
		
	}

	@Override
	public void render()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cleanUp()
	{
		// TODO Auto-generated method stub
		
	}
	
	public Item getItemById(String id)
	{
		for(Item i : items)
		{
			if(i.getId().equalsIgnoreCase(id)) return i;
		}
		return null;
	}

}
