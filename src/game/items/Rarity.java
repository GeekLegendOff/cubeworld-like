package game.items;

import engine.utils.Color4f;

public enum Rarity
{
	
	COMMON("Common", new Color4f(1f, 1f, 1f)),
	UNCOMMON("Uncommon", new Color4f(0.5f, 1f, 0f)),
	RARE("Rare", new Color4f(0.25f, 0.25f, 1f)),
	EPIC("Epic", new Color4f(0.5f, 0f, 1f)),
	LEGENDARY("Legendary", new Color4f(0f, 0.75f, 1f)),
	ARTIFACT("Artifact", new Color4f(1f, 0f, 0f)),
	SPECIAL("Special", new Color4f(1f, 0.25f, 0.5f));
	
	private String label;
	private Color4f color;
	
	private Rarity(String label, Color4f color)
	{
		this.label = label;
		this.color = color;
	}

	public String getLabel()
	{
		return label;
	}
	
	public Color4f getColor()
	{
		return color;
	}
	
}
