package game.items.placeable.blocks.basics;

import engine.utils.Color4f;
import game.Game;
import game.items.placeable.blocks.BlockItem;

public class GrassItem extends BlockItem
{

	public GrassItem(int x, int z)
	{
		super("grass");
		setName("Grass");
		setDescription("Take some dirt and add some green.");
		Color4f ca = new Color4f(0.05f, 0.4f, 0.05f);
		Color4f cb = new Color4f(0.1f, 0.5f, 0.1f);
		float t = Game.getInstance().getGameRenderer().getWorldManager().getWorldNoise().getHeight(x, z) / 30.0f;
		Color4f color = Color4f.mix(ca, cb, t);
		color.setA(1.0f);
		getBlock()
			.setColor(color)
			.setResistance(1f);
	}
	
	public GrassItem()
	{
		super("grass");
		setName("Grass");
		setDescription("Take some dirt and add some green.");
		getBlock()
			.setColor(new Color4f(0.1f, 0.5f, 0.1f, 1.0f))
			.setResistance(1f);
	}
	
}
