package game.items.placeable.blocks.basics;

import engine.utils.Color4f;
import game.items.placeable.blocks.BlockItem;

public class BeechLeafItem extends BlockItem
{

	public BeechLeafItem()
	{
		super("beech_leaf");
		setName("Beech Leaf");
		setDescription("Leaves, what use?");
		getBlock()
			.setColor(new Color4f(0.15f, 0.45f, 0.15f, 1.0f))
			.setResistance(1.2f)
			.clearDrops();
	}

}
