package game.items.placeable.blocks.basics;

import engine.utils.Color4f;
import game.items.consumables.food.basics.AppleItem;
import game.items.placeable.blocks.BlockItem;

public class AppleLeafItem extends BlockItem
{
	
	public AppleLeafItem()
	{
		super("apple_leaf");
		setName("Apple Leaf");
		setDescription("Contains apple!");
		getBlock()
			.setColor(new Color4f(0.7f, 0.2f, 0.2f, 1.0f))
			.setResistance(1.2f)
			.clearDrops()
			.addDrop(new AppleItem());
	}

}
