package game.items.placeable.blocks.basics;

import engine.utils.Color4f;
import game.items.placeable.blocks.BlockItem;

public class OakLeafItem extends BlockItem
{

	public OakLeafItem()
	{
		super("oak_leaf");
		setName("Oak Leaf");
		setDescription("Leaves, what use?");
		getBlock()
			.setColor(new Color4f(0.1f, 0.4f, 0.1f, 1.0f))
			.setResistance(1.2f)
			.clearDrops();
	}

}
