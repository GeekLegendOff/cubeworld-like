package game.items.placeable.blocks.basics;

import engine.utils.Color4f;
import game.items.placeable.blocks.BlockItem;

public class WaterItem extends BlockItem
{

	public WaterItem()
	{
		super("water");
		setName("Water");
		setDescription("Liquid but solid water?");
		getBlock()
			.setColor(new Color4f(0.196078f, 0.6f, 0.8f, 0.5f))
			.setResistance(-1f);
	}

}
