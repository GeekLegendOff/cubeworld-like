package game.items.placeable.blocks.basics;

import engine.utils.Color4f;
import game.items.placeable.blocks.BlockItem;

public class AppleTreeLeafItem extends BlockItem
{
	
	public AppleTreeLeafItem()
	{
		super("apple_tree_leaf");
		setName("Apple Tree Leaf");
		setDescription("Leaves, what use?");
		getBlock()
			.setColor(new Color4f(0.14f, 0.44f, 0.14f, 1.0f))
			.setResistance(1.2f)
			.clearDrops();
	}

}
