package game.items.placeable.blocks.basics;

import engine.utils.Color4f;
import game.items.placeable.blocks.BlockItem;

public class CloudItem extends BlockItem
{

	public CloudItem()
	{
		super("cloud");
		setName("Cloud");
		setDescription("A cloud, only a cloud.");
		getBlock()
			.setColor(new Color4f(1.0f, 1.0f, 1.0f, 1.0f))
			.setResistance(0.5f)
			.clearDrops();
	}

}
