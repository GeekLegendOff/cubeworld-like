package game.items.placeable.blocks.basics;

import engine.utils.Color4f;
import game.items.placeable.blocks.BlockItem;

public class WoodItem extends BlockItem
{

	public WoodItem()
	{
		super("wood");
		setName("Wood");
		setDescription("The raw material of almost everything.");
		getBlock()
			.setColor(new Color4f(0.252f, 0.192f, 0.08400001f, 1.0f))
			.setResistance(2f);
	}

}
