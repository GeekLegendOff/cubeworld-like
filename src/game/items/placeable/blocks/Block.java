package game.items.placeable.blocks;

import java.util.HashMap;
import java.util.Map;

import engine.utils.Color4f;
import game.Game;
import game.items.Item;

public class Block
{
	
	private static int SIZE = 1;
	
	private Map<Item, Integer> drops = new HashMap<Item, Integer>();	
	private String id;
	private float resistance;
	private Color4f color;
	
	public Block(String id)
	{
		this.id = id;
	}
	
	public String getId()
	{
		return id;
	}
	public float getResistance()
	{
		return resistance;
	}
	public Block setResistance(float resistance)
	{
		this.resistance = resistance;
		return this;
	}
	public Color4f getColor()
	{
		return color;
	}
	public Block setColor(Color4f color)
	{
		this.color = color;
		return this;
	}
	public Block addDrop(Item item, int chance)
	{
		drops.put(item, chance);
		return this;
	}
	public Block addDrop(Item item)
	{
		drops.put(item, 100);
		return this;
	}
	public Map<Item, Integer> getDrops()
	{
		return drops;
	}
	public Block setDrops(Map<Item, Integer> drops)
	{
		this.drops = drops;
		return this;
	}
	public Block clearDrops()
	{
		drops.clear();
		return this;
	}
	
	public static Block getBlockById(String id) {
		Item i = Game.getInstance().getGameRenderer().getWorldManager().getItemManager().getItemById(id);
		if(i instanceof BlockItem)
			return ((BlockItem) i).getBlock();
		return null;
	}
	
	public static float[] getFrontData(float x, float y, float z) {
		return new float[] {
				x, y, z,
				x, y + SIZE, z,
				x + SIZE, y + SIZE, z,
				x + SIZE, y, z,
		};
	}
	
	public static float[] getBackData(float x, float y, float z) {
		return new float[] {
				x, y, z + SIZE,
				x, y + SIZE, z + SIZE,
				x + SIZE, y + SIZE, z + SIZE,
				x + SIZE, y, z + SIZE,
		};
	}
	
	public static float[] getRightData(float x, float y, float z) {
		return new float[] {
				x + SIZE, y, z,
				x + SIZE, y + SIZE, z,
				x + SIZE, y + SIZE, z + SIZE,
				x + SIZE, y, z + SIZE,
		};
	}
	
	public static float[] getLeftData(float x, float y, float z) {
		return new float[] {
				x, y, z,
				x, y + SIZE, z, 
				x, y + SIZE, z + SIZE,
				x, y, z + SIZE,
		};
	}
	
	public static float[] getUpData(float x, float y, float z) {
		return new float[] {
				x, y + SIZE, z,
				x, y + SIZE, z + SIZE,
				x + SIZE, y + SIZE, z + SIZE,
				x + SIZE, y + SIZE, z,
		};
	}
	
	public static float[] getDownData(float x, float y, float z) {
		return new float[] {
				x, y, z,
				x, y, z + SIZE,
				x + SIZE, y, z + SIZE,
				x + SIZE, y, z,
		};
	}
	
	public static float[] getFrontColorData(Color4f color, float brightness, float[] shading) {
		float coef = 0.9f;
		
		float s0 = shading[0] * brightness;
		float s1 = shading[1] * brightness;
		float s2 = shading[2] * brightness;
		float s3 = shading[3] * brightness;
		
		return new float[] {
				color.r * coef * s1, color.g * coef * s1, color.b * coef * s1, color.a,
				color.r * coef * s0, color.g * coef * s0, color.b * coef * s0, color.a,
				color.r * coef * s3, color.g * coef * s3, color.b * coef * s3, color.a,
				color.r * coef * s2, color.g * coef * s2, color.b * coef * s2, color.a,
		};
	}
	
	public static float[] getBackColorData(Color4f color, float brightness, float[] shading) {
		float coef = 0.9f;
		
		float s0 = shading[0] * brightness;
		float s1 = shading[1] * brightness;
		float s2 = shading[2] * brightness;
		float s3 = shading[3] * brightness;
		
		return new float[] {
				color.r * coef * s1, color.g * coef * s1, color.b * coef * s1, color.a,
				color.r * coef * s0, color.g * coef * s0, color.b * coef * s0, color.a,
				color.r * coef * s3, color.g * coef * s3, color.b * coef * s3, color.a,
				color.r * coef * s2, color.g * coef * s2, color.b * coef * s2, color.a,
		};
	}
	
	public static float[] getRightColorData(Color4f color, float brightness, float[] shading) {
		float coef = 0.8f;
		
		float s0 = shading[0] * brightness;
		float s1 = shading[1] * brightness;
		float s2 = shading[2] * brightness;
		float s3 = shading[3] * brightness;
		
		return new float[] {
				color.r * coef * s1, color.g * coef * s1, color.b * coef * s1, color.a,
				color.r * coef * s0, color.g * coef * s0, color.b * coef * s0, color.a,
				color.r * coef * s3, color.g * coef * s3, color.b * coef * s3, color.a,
				color.r * coef * s2, color.g * coef * s2, color.b * coef * s2, color.a,
		};
	}
	
	public static float[] getLeftColorData(Color4f color, float brightness, float[] shading) {
		float coef = 0.8f;
		
		float s0 = shading[0] * brightness;
		float s1 = shading[1] * brightness;
		float s2 = shading[2] * brightness;
		float s3 = shading[3] * brightness;
		
		return new float[] {
				color.r * coef * s1, color.g * coef * s1, color.b * coef * s1, color.a,
				color.r * coef * s0, color.g * coef * s0, color.b * coef * s0, color.a,
				color.r * coef * s3, color.g * coef * s3, color.b * coef * s3, color.a,
				color.r * coef * s2, color.g * coef * s2, color.b * coef * s2, color.a,
		};
	}
	
	public static float[] getUpColorData(Color4f color, float brightness, float[] shading) {
		float coef = 1.0f;
		
		float s0 = shading[0] * brightness;
		float s1 = shading[1] * brightness;
		float s2 = shading[2] * brightness;
		float s3 = shading[3] * brightness;
		
		return new float[] {
				color.r * coef * s1, color.g * coef * s1, color.b * coef * s1, color.a,
				color.r * coef * s0, color.g * coef * s0, color.b * coef * s0, color.a,
				color.r * coef * s3, color.g * coef * s3, color.b * coef * s3, color.a,
				color.r * coef * s2, color.g * coef * s2, color.b * coef * s2, color.a,
		};
	}
	
	public static float[] getDownColorData(Color4f color, float brightness, float[] shading) {
		float coef = 0.7f;
		
		float s0 = shading[0] * brightness;
		float s1 = shading[1] * brightness;
		float s2 = shading[2] * brightness;
		float s3 = shading[3] * brightness;
		
		return new float[] {
				color.r * coef * s1, color.g * coef * s1, color.b * coef * s1, color.a,
				color.r * coef * s0, color.g * coef * s0, color.b * coef * s0, color.a,
				color.r * coef * s3, color.g * coef * s3, color.b * coef * s3, color.a,
				color.r * coef * s2, color.g * coef * s2, color.b * coef * s2, color.a,
		};
	}

}
