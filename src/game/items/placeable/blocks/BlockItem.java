package game.items.placeable.blocks;

import game.items.placeable.PlaceableItem;

public abstract class BlockItem extends PlaceableItem
{
	private Block block;
	
	public BlockItem(String id)
	{
		super(id);
		block = new Block(id);
		block.addDrop(this, 100);
	}
	
	public Block getBlock()
	{
		return block;
	}
		
}
