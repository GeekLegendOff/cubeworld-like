package game.items.placeable;

import game.items.Item;

public abstract class PlaceableItem extends Item
{

	public PlaceableItem(String id)
	{
		super(id);
	}

}
