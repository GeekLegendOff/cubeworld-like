package game.items;

import java.util.UUID;

import engine.rendering.model.OBJModel;

public abstract class Item 
{
	private String id;
	private UUID uuid;
	private String name = "item." + id;
	private String displayName;
	private String lore;
	private String description = "";
	private int stacksize = 100;
	private OBJModel model;
	private Rarity rarity = Rarity.COMMON;
	
	public Item(String id)
	{
		this.id = id;
		uuid = UUID.randomUUID();
	}
	
	protected void setName(String name)
	{
		this.name = name;
	}

	protected void setDescription(String description)
	{
		this.description = description;
	}

	protected void setStacksize(int stacksize)
	{
		this.stacksize = stacksize;
	}

	protected void setRarity(Rarity rarity)
	{
		this.rarity = rarity;
	}
	
	protected OBJModel setModel(String path)
	{
		return new OBJModel(path);
	}

	public void render()
	{
		this.model.render();
	}
	
	public void update() {};
	
	public Item clone() {
		try
		{
			Item newItem = (Item) super.clone();
			newItem.regenUuid();
			return newItem;
		} catch (CloneNotSupportedException e)
		{}
		return null;
	}
	
	public void regenUuid() {
		uuid = UUID.randomUUID();
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public String getLore()
	{
		return lore;
	}

	public void setLore(String lore)
	{
		this.lore = lore;
	}

	public String getId()
	{
		return id;
	}

	public UUID getUuid()
	{
		return uuid;
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public int getStacksize()
	{
		return stacksize;
	}

	public OBJModel getModel()
	{
		return model;
	}

	public Rarity getRarity()
	{
		return rarity;
	}
	
}
