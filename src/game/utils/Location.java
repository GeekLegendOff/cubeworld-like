package game.utils;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class Location
{
	public float x, y, z, pitch, yaw;
	
	public Location() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
	
	public Location(float x, float y, float z)
	{
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Location(float x, float y, float z, float pitch, float yaw)
	{
		super();
		this.x = x;
		this.y = y;
		this.z = z;
		this.pitch = pitch;
		this.yaw = yaw;
	}
	
	public Location(Vector3f v) {
		super();
		this.x = v.x;
		this.y = v.y;
		this.z = v.z;
	}

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public float getZ()
	{
		return z;
	}

	public void setZ(float z)
	{
		this.z = z;
	}

	public float getYaw()
	{
		return yaw;
	}

	public void setYaw(float yaw)
	{
		this.yaw = yaw;
	}

	public float getPitch()
	{
		return pitch;
	}

	public void setPitch(float pitch)
	{
		this.pitch = pitch;
	}
	
	public void zero()
	{
		setX(0);
		setY(0);
		setZ(0);
		setPitch(0);
		setY(0);
	}
	
	public void invert() {
		
	}
	
	public void addX(float a)
	{
		x += a;
	}
	
	public void addY(float a)
	{
		y += a;
	}
	
	public void addZ(float a)
	{
		z += a;
	}
	
	public void addPitch(float a)
	{
		pitch += a;
	}
	
	public void addYaw(float a)
	{
		yaw += a;
	}
	
	public void add(Location loc)
	{
		addX(loc.getX());
		addY(loc.getY());
		addZ(loc.getZ());
		addPitch(loc.getPitch());
		addYaw(loc.getYaw());
	}
	
	public void add(float x, float y, float z, float pitch, float yaw)
	{
		add(new Location(x, y, z, pitch, yaw));
	}
	
	public void add(float x, float y, float z)
	{
		add(x, y, z, 0, 0);
	}
	
	public void sub(Location loc)
	{
		add(-loc.getX(), -loc.getY(), -loc.getZ(), -loc.getPitch(), -loc.getYaw());
	}
	
	public void sub(float x, float y, float z, float pitch, float yaw)
	{
		sub(new Location(x, y, z, pitch, yaw));
	}
	
	public void sub(float x, float y, float z)
	{
		sub(x, y, z, 0, 0);
	}
	
	public void multiplyX(float value) {
		setX(getX() * value);
	}
	
	public void multiplyY(float value) {
		setY(getY() * value);
	}
	
	public void multiplyZ(float value) {
		setZ(getZ() * value);
	}
	
	public void multiplyPitch(float value) {
		setPitch(getPitch() * value);
	}
	
	public void multiplyYaw(float value) {
		setYaw(getYaw() * value);
	}
	
	public void multiply(float value)
	{
		multiplyX(value);
		multiplyY(value);
		multiplyZ(value);
		multiplyPitch(value);
		multiplyYaw(value);
	}
	
    public Vector3f getDirection() {
        Vector3f vector = new Vector3f();

        double rotX = this.getYaw();
        double rotY = this.getPitch();

        vector.y = (float) -Math.sin(Math.toRadians(rotY));

        double xz = (float) Math.cos(Math.toRadians(rotY));

        vector.x = (float) (-xz * Math.sin(Math.toRadians(rotX)));
        vector.z = (float) (xz * Math.cos(Math.toRadians(rotX)));

        return vector;
    }
    
    public Location setDirection(Vector3f vector) {
        final double _2PI = 2 * Math.PI;
        final double x = vector.x;
        final double z = vector.x;

        if (x == 0 && z == 0) {
            pitch = vector.y > 0 ? -90 : 90;
            return this;
        }

        double theta = Math.atan2(-x, z);
        setYaw((float) Math.toDegrees((theta + _2PI) % _2PI));

        double x2 = x*x;
        double z2 = z*z;
        double xz = Math.sqrt(x2 + z2);
        setPitch((float) Math.toDegrees(Math.atan(-vector.y / xz)));

        return this;
    }
    
    public double length() {
        return Math.sqrt(lengthSquared());
    }
    
    public double lengthSquared() {
        return getX()*getX() + getY()*getY() + getZ()*getZ();
    }
    
    public double distance(Location loc) {
        return Math.sqrt(distanceSquared(loc));
    }
    
    public double distanceSquared(Location loc) {
        if (loc == null) {
            throw new IllegalArgumentException("Cannot measure distance to a null location");
        }
        return (getX() - loc.getX())*(getX() - loc.getX()) + (getY() - loc.y)*(getY() - loc.y) + (getZ() - loc.z)*(getZ() - loc.z);
    }
    
    public Vector3f toVector()
	{
		return new Vector3f(getX(), getY(), getZ());
	}
	
	@Override
	public String toString()
	{
		return "[x: " + x + ", y: " + y + ", z: " + z + ", pitch: " + pitch + ", yaw: " + yaw + "]";
	}
	
	public static Vector2f rotToVector(Location loc) {
		return new Vector2f(loc.pitch, loc.yaw);
	}
	
    public static float normalizeYaw(float yaw) {
        yaw %= 360.0f;
        if (yaw >= 180.0f) {
            yaw -= 360.0f;
        } else if (yaw < -180.0f) {
            yaw += 360.0f;
        }
        return yaw;
    }
    
    public static float normalizePitch(float pitch) {
        if (pitch > 90.0f) {
            pitch = 90.0f;
        } else if (pitch < -90.0f) {
            pitch = -90.0f;
        }
        return pitch;
    }
	
}
