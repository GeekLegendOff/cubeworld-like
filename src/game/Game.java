package game;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import engine.game.IGameLogic;
import engine.inputs.Input;
import game.rendering.renderers.GameRenderer;
import game.rendering.window.Window;
import game.world.environment.trees.Tree;

public class Game implements IGameLogic
{

	private static Game instance;

	private Window window;

	private GameRenderer gameRenderer;
	
	public Game()
	{
		this.window = new Window(1280, 720, "Development Build", false, false, false);
		this.gameRenderer = new GameRenderer();
	}

	public void start()
	{
		instance = this;
		Tree.registerTrees();
		window.create();
		gameRenderer.init();
		loop();
	}

	public void stop()
	{
		instance = null;
		gameRenderer.cleanUp();
		window.destroy();
		System.exit(0);
	}

	private void loop()
	{
		long before = System.nanoTime();
		long timer = System.currentTimeMillis();

		double ns = 1000000000.0D / 60.0D;

		@SuppressWarnings("unused")
		int ups = 0, fps = 0;

		while (!window.isCloseRequested())
		{
			long now = System.nanoTime();

			if (now - before > ns)
			{
				before += ns;
				ups++;
				update();
			} else
			{
				fps++;
				render();
				window.update();
			}
			if (System.currentTimeMillis() - timer > 1000)
			{
				timer += 1000;
				ups = 0;
				fps = 0;
			}
		}
		stop();
	}

	@Override
	public void init()
	{
		gameRenderer.init();
	}
	
	@Override
	public void render()
	{
		window.clearBuffers();
		
		gameRenderer.render();
	}

	@Override
	public void update()
	{
		Input.update();
		
		if (Mouse.isButtonDown(0) && !Mouse.isGrabbed())
		{
			Mouse.setGrabbed(true);
		}
		if (Input.getKeyDown(Keyboard.KEY_ESCAPE) && Mouse.isGrabbed())
		{
			Mouse.setGrabbed(false);
		}
		if (!Mouse.isGrabbed())
		{
			return;
		}
		if (Input.getKeyDown(Keyboard.KEY_F11))
		{
			window.setFullscreen();
		}
		
		gameRenderer.update();
	}
	
	public static Game getInstance()
	{
		return instance;
	}

	@Override
	public void cleanUp()
	{
		
	}
	
	public Window getWindow()
	{
		return window;
	}
	
	public GameRenderer getGameRenderer()
	{
		return gameRenderer;
	}

}
