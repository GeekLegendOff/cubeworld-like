package game.particules;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import engine.game.IGameLogic;
import engine.maths.Mathf;
import engine.utils.Color4f;

public class Particule implements IGameLogic
{
	
	private Vector3f position;
	private Vector3f rotation = new Vector3f().zero();
	
	private float size = 1.0f;
	private Color4f color;
	
	private Vector3f bottomBackLeft, bottomBackRight, bottomFrontLeft, bottomFrontRight, topBackLeft, topBackRight, topFrontLeft, topFrontRight;
	
	public Particule(Vector3f position, Vector3f rotation, Color4f color)
	{
		this.position = position;
		this.rotation = rotation;
		this.color = color;
	}

	public Particule(Vector3f position, Color4f color)
	{
		this.position = position;
		this.color = color;
	}

	@Override
	public void init()
	{
		this.bottomBackLeft = new Vector3f().zero();
		this.bottomBackRight = new Vector3f().zero();
		this.bottomFrontLeft = new Vector3f().zero();
		this.bottomFrontRight = new Vector3f().zero();
		this.topBackLeft = new Vector3f().zero();  
		this.topBackRight = new Vector3f().zero(); 
		this.topFrontLeft = new Vector3f().zero(); 
		this.topFrontRight = new Vector3f().zero();
		
		update();
	}

	@Override
	public void render()
	{
		
	}

	@Override
	public void update()
	{
		bottomBackLeft.x = position.x + size * -0.5f;
		bottomBackLeft.y = position.y + size * -0.5f;
		bottomBackLeft.z = position.z + size * -0.5f;
		
		bottomBackRight.x = position.x + size * 0.5f;
		bottomBackRight.y = position.y + size * -0.5f;
		bottomBackRight.z = position.z + size * -0.5f;
		
		bottomFrontLeft.x = position.x + size * -0.5f;
		bottomFrontLeft.y = position.y + size * -0.5f;
		bottomFrontLeft.z = position.z + size * 0.5f;
		
		bottomFrontRight.x = position.x + size * 0.5f;
		bottomFrontRight.y = position.y + size * -0.5f;
		bottomFrontRight.z = position.z + size * 0.5f;
		
		topBackLeft.x = position.x + size * -0.5f; 
		topBackLeft.y = position.y + size * 0.5f; 
		topBackLeft.z = position.z + size * -0.5f; 
		                                       
		topBackRight.x = position.x + size * 0.5f; 
		topBackRight.y = position.y + size * 0.5f;
		topBackRight.z = position.z + size * -0.5f;
		                                      
		topFrontLeft.x = position.x + size * -0.5f;
		topFrontLeft.y = position.y + size * 0.5f;
		topFrontLeft.z = position.z + size * 0.5f; 
		                                       
		topFrontRight.x = position.x + size * 0.5f;
		topFrontRight.y = position.y + size * 0.5f;
		topFrontRight.z = position.z + size * 0.5f;
		
		Matrix4f m = new Matrix4f();
		m.translate(position)
		 .scale(size)
		 .rotateXYZ(Mathf.toRadians(rotation));
		
		m.transformPosition(bottomBackLeft);   
		m.transformPosition(bottomBackRight);  
		m.transformPosition(bottomFrontLeft);  
		m.transformPosition(bottomFrontRight); 
		m.transformPosition(topBackLeft);      
		m.transformPosition(topBackRight);     
		m.transformPosition(topFrontLeft);     
		m.transformPosition(topFrontRight);    
	}

	@Override
	public void cleanUp()
	{
		
	}
	
	public Vector3f getPosition()
	{
		return position;
	}
	
	public void setPosition(Vector3f position)
	{
		this.position = position;
	}
	
	public Vector3f getRotation()
	{
		return rotation;
	}
	
	public void setRotation(Vector3f rotation)
	{
		this.rotation = rotation;
	}

	public float getSize()
	{
		return size;
	}

	public void setSize(float size)
	{
		this.size = size;
	}

	public float[] getBottomData() {
		return new float[] {
				bottomFrontLeft.x,  bottomFrontLeft.y,  bottomFrontLeft.z,
				bottomBackLeft.x,   bottomBackLeft.y,   bottomBackLeft.z,
				bottomBackRight.x,  bottomBackRight.y,  bottomBackRight.z,
				bottomFrontRight.x, bottomFrontRight.y, bottomFrontRight.z,
		};
	}
	
	public float[] getBackData() {
		return new float[] {
				bottomBackLeft.x,  bottomBackLeft.y,   bottomBackLeft.z,
				bottomBackRight.x, bottomBackRight.y,  bottomBackRight.z,
				topBackRight.x,    topBackRight.y,     topBackRight.z,
				topBackLeft.x,     topBackLeft.y,      topBackLeft.z,
		};
	}
	
	public float[] getFrontData() {
		return new float[] {
				bottomFrontLeft.x,  bottomFrontLeft.y,   bottomFrontLeft.z,
				bottomFrontRight.x, bottomFrontRight.y,  bottomFrontRight.z,
				topFrontRight.x,    topFrontRight.y,     topFrontRight.z,
				topFrontLeft.x,     topFrontLeft.y,      topFrontLeft.z,
		};
	}
	
	public float[] getLeftData() {
		return new float[] {
				bottomFrontLeft.x, bottomFrontLeft.y, bottomFrontLeft.z,
				bottomBackLeft.x,  bottomBackLeft.y,  bottomBackLeft.z,
				topBackLeft.x,     topBackLeft.y,     topBackLeft.z,
				topFrontLeft.x,    topFrontLeft.y,    topFrontLeft.z,
		};
	}
	
	public float[] getRightData() {
		return new float[] {
				bottomFrontRight.x, bottomFrontRight.y, bottomFrontRight.z,
				bottomBackRight.x,  bottomBackRight.y,  bottomBackRight.z,
				topBackRight.x,     topBackRight.y,     topBackRight.z,
				topFrontRight.x,    topFrontRight.y,    topFrontRight.z,
		};
	}
	
	public float[] getTopData() {
		return new float[] {
				topFrontLeft.x,  topFrontLeft.y,  topFrontLeft.z,
				topBackLeft.x,   topBackLeft.y,   topBackLeft.z,
				topBackRight.x,  topBackRight.y,  topBackRight.z,
				topFrontRight.x, topFrontRight.y, topFrontRight.z,
		};
	}
	
	public float[] getColorData() {
		return new float[] {
				color.r, color.g, color.b, color.a,
				color.r, color.g, color.b, color.a,
				color.r, color.g, color.b, color.a,
				color.r, color.g, color.b, color.a,
		};
	}

}
