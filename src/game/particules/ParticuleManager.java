package game.particules;

import static org.lwjgl.opengl.GL11.GL_COLOR_ARRAY;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_VERTEX_ARRAY;
import static org.lwjgl.opengl.GL11.glColorPointer;
import static org.lwjgl.opengl.GL11.glDisableClientState;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glEnableClientState;
import static org.lwjgl.opengl.GL11.glVertexPointer;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;

import engine.game.IGameLogic;

public class ParticuleManager implements IGameLogic
{

	private List<Particule> particules = new ArrayList<Particule>();
	private float multiplier;

	private int vbo, cbo;
	private int bufferSize;
	private FloatBuffer vertexBuffer, vertexColorBuffer;

	public ParticuleManager(float multiplier)
	{
		this.multiplier = multiplier;
	}

	@Override
	public void init()
	{
		createBuffer();
	}

	@Override
	public void render()
	{
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glVertexPointer(3, GL_FLOAT, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, cbo);
		glColorPointer(4, GL_FLOAT, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glDrawArrays(GL_QUADS, 0, bufferSize);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
	}

	@Override
	public void update()
	{
		for (Particule p : particules)
		{
			p.update();
		}
	}

	@Override
	public void cleanUp()
	{
		for (Particule p : particules)
		{
			p.cleanUp();			
		}
	}

	private void createBuffer()
	{
		vertexBuffer = BufferUtils.createFloatBuffer(particules.size() * 6 * 4 * 3);
		vertexColorBuffer = BufferUtils.createFloatBuffer(particules.size() * 6 * 4 * 4);
		
		vbo = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		cbo = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, cbo);
		glBufferData(GL_ARRAY_BUFFER, vertexColorBuffer, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		for (Particule p : particules)
		{
			vertexBuffer.put(p.getTopData());
			vertexBuffer.put(p.getBottomData());
			vertexBuffer.put(p.getRightData());
			vertexBuffer.put(p.getLeftData());
			vertexBuffer.put(p.getFrontData());
			vertexBuffer.put(p.getBackData());
			
			vertexColorBuffer.put(p.getColorData());
		}
		
		vertexBuffer.clear();
		vertexColorBuffer.clear();
	}
	
	public void updateBuffers()
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_STATIC_DRAW);	
		
		glBindBuffer(GL_ARRAY_BUFFER, cbo);
		glBufferData(GL_ARRAY_BUFFER, vertexColorBuffer, GL_STATIC_DRAW);	
	}

	public int addParticule(Particule particule)
	{
		particule.init();
		particules.add(particule);
		return particules.indexOf(particule);
	}

	public void removeParticule(int id)
	{
		particules.get(id).cleanUp();
		particules.remove(id);
	}

}
